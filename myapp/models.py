from cms.models.pluginmodel import CMSPlugin

from django.db import models
from aldryn_newsblog.models import Article
from filer import settings as filer_settings
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField
from cms.models import CMSPlugin 

class Hello(CMSPlugin):
	guest_name = models.CharField(max_length=50, default='Guset')

class NewsPDF(CMSPlugin):
	title = models.CharField(max_length=50, blank=True, null=True, default=None)
	file = FilerFileField(null=True, blank=True, related_name="pdf_disclaimer")

	def __unicode__(self):
		return self.title

class FullImage(CMSPlugin):
	title = models.CharField(max_length=50, blank=True, null=True, default=None)
	file = FilerImageField(null=True, blank=True, related_name="image_disclaimer")

	def __unicode__(self):
		return self.title

class VideoPluginEditor(CMSPlugin): 	
	logo = FilerImageField(null=True, blank=True, related_name="company_logo")
	file = FilerFileField(null=True, blank=True, related_name="company_disclaimer")
	videoURL = models.URLField(max_length=200,null=True, blank=True, default=None)
	title = models.CharField(max_length=20)
	width = models.CharField(max_length=10)
	height = models.CharField(max_length=10)
