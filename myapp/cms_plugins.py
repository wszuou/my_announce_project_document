from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .models import Hello, VideoPluginEditor, NewsPDF, FullImage

class HelloPlugin(CMSPluginBase):
    model = Hello
    name = _("Hello Plugin")
    render_template = "hello_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HelloPlugin, self).render(context, instance, placeholder)
        return context

class VideoPluginPublisher(CMSPluginBase):
	model = VideoPluginEditor
	render_template = "video/video.html"
	name = _("video Pub")
	text_enable = True
	admin_preview = False

	def icon_src(self, instance):
		return "/static/plugin_icons/video.png"

	def render(self, context, instance, placeholder):
		context.update({
			'video':instance,
			'placeholder':placeholder,
			})
		return context

class NewsPluginPDF(CMSPluginBase):
	model = NewsPDF
	render_template = "pdf_plugin.html"
	name = _("pdf news")
	text_enable = True
	admin_preview = False

	def render(self, context, instance, placeholder):
		# check format file is pdf
		context.update({
			'pdf_news':instance,
			'placeholder':placeholder,
			})
		return context

class FullImagePlugin(CMSPluginBase):
	model = FullImage
	render_template = "full_image.html"
	name = _("full image")
	text_enable = True
	admin_preview = False

	def render(self, context, instance, placeholder):
		context.update({
			'full_image':instance,
			'placeholder':placeholder,
			})
		return context

class time_controller(CMSPluginBase):
	render_template = "time_controller.html"
	name = _("time_controller")
	text_enable = True
	admin_preview = False

	def render(self, context, instance, placeholder):
		# check format file is pdf
		context.update({
			'placeholder':placeholder,
			})
		return context


plugin_pool.register_plugin(HelloPlugin)
plugin_pool.register_plugin(VideoPluginPublisher)
plugin_pool.register_plugin(NewsPluginPDF)
plugin_pool.register_plugin(time_controller)
plugin_pool.register_plugin(FullImagePlugin)