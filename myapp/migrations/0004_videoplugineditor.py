# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('filer', '0002_auto_20150606_2003'),
        ('myapp', '0003_auto_20160329_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoPluginEditor',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(max_length=20)),
                ('width', models.CharField(max_length=10)),
                ('height', models.CharField(max_length=10)),
                ('file', filer.fields.file.FilerFileField(to='filer.File', null=True, blank=True, related_name='company_disclaimer')),
                ('logo', filer.fields.image.FilerImageField(to='filer.Image', null=True, blank=True, related_name='company_logo')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
