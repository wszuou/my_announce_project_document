from django.contrib import admin
from image.models import Upload


class UploadAdmin(admin.ModelAdmin):
    list_display = ('image_tag', 'add_date', 'user', 'height', 'width')
    list_filter = ['add_date']

admin.site.register(Upload, UploadAdmin)