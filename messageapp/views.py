#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
import json
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render
#from django.utils import timezone
#from django.utils.timezone import localtime 
import datetime
from datetime import datetime, timedelta
from django.views.decorators.csrf import ensure_csrf_cookie
from viewer.models import UserViewer
from django.contrib.auth.decorators import permission_required, login_required, user_passes_test
from django.utils.decorators import method_decorator
from .models import MessageAnnounce
#@ensure_csrf_cookie
@login_required
def post_message(request): # not use
	"""
	1. sort MessageAnnounce by start_time. lastest come first
	2. Check each instance collect all message that was published
		and then show message that in publish time
	"""
	template_name = 'messageapp/post.html'
	all_message = MessageAnnounce.objects.order_by('-start_time') 
	message_published = []
	my_time = datetime.now()#localtime(timezone.now())
	for message in all_message:
		if message.was_published():
			message_published.append(message)
	if len(message_published) != 0:
		return render(request, template_name, {'my_time':my_time, 'message_posted':message_published.pop()})
	else:
		return render(request, template_name, {'my_time':my_time})

@login_required
def posted(request):
	"""
	1. get message to create instance.
	2. Check time if in latest message's published time then plus 31 secounds start_time.
	3. else use local timezone.now()
	"""
	message_geten = request.POST['message']
	user = request.user
	user_name = str(user.username)
	full_name = user.first_name+'  '+user.last_name
	if not (message_geten.isspace() or message_geten == "") :
		try:
			latest_message = MessageAnnounce.objects.order_by('-start_time')
			message_published = []
			for message in latest_message:
				if message.was_published():
					message_published.append(message)
					break
			if len(message_published) != 0: # if has published message then use coutinue time from that message.
				print("))))))))))))))))))))  new message")
				new_post = MessageAnnounce(message=message_geten,
					start_time=latest_message[0].start_time+timedelta(seconds=31),
					end_time=latest_message[0].start_time+timedelta(seconds=61),
					writer=user_name, text_owner=full_name, create_at=datetime.now()) 
				new_post.save()
			else:
				create_message(message_geten, user_name, full_name)
		except IndexError:
			create_message(message_geten, user_name, full_name)
	return HttpResponseRedirect(reverse('viewer:main_page')) # go to main page.

def create_message(message_geten, user_name, full_name):
	new_post = MessageAnnounce(message=message_geten,
		start_time=datetime.now(),#localtime(timezone.now()),
		end_time=datetime.now()+timedelta(seconds=30),
		writer=user_name, text_owner=full_name, create_at=datetime.now() )#localtime(timezone.now())+datetime.timedelta(seconds=30))
	new_post.save()

def message_page(request): # not use
	template_name = 'messageapp/message.html'
	return render(request, template_name, {})

def update_message(request):
	from random import randint
	client_message = json.loads(request.GET.get('client_data'))
	# send present message back to client
	all_message = MessageAnnounce.objects.order_by('-start_time') 
	message_published = []
	my_time = datetime.now() #localtime(timezone.now())

	for message in all_message:
		if message.was_published():
			message_published.append(message)
			break

	if len(message_published) != 0: # send message and time
		print(">>>>>>>>>>>>>>>>>>>>>> meet message")
		message_server = message_published.pop()
		message_news = str(message_server.message) + u' จาก '+ str(message_server.text_owner)
		server_data = {'server_data':[
				{'message':message_news},
				{'message':str(my_time).split(' ')[1].split('.')[0]},
				{'message':str(message_server.get_message_time() )},
		]}
	else: # send only time.
		
		if MessageAnnounce.objects.order_by('-create_at')[:10]: # if there are latest 10 messages then random show them.
			print("================= Random message")
			len_msg = len(MessageAnnounce.objects.order_by('-create_at')[:10])
			latest_message = MessageAnnounce.objects.order_by('-create_at')[:len_msg]
			ran_num = randint(0, len_msg-1) # random index of message
			latest_message = latest_message[ran_num]
			latest_message.start_time = datetime.now()
			latest_message.end_time = datetime.now()+timedelta(seconds=30)
			latest_message.save()
			if latest_message.writer != "admin": # if not be admin show writeer name
				message_news = str(latest_message.message) + u' จาก '+ str(latest_message.text_owner)
			else: # if be admin not show writer name
				message_news = str(latest_message.message)
		else: # if be admin not show writer name
			no_message = MessageAnnounce.objects.get(message=u'ท่านสามารถแสดงความคิดเห็นได้ที่นี่')
			message_news = str(no_message.message)
		
		server_data = {'server_data':[
				{'message':message_news},
				{'message':str(my_time).split(' ')[1].split('.')[0]},
				{'message':'1000'},
		]}
	return JsonResponse(server_data)