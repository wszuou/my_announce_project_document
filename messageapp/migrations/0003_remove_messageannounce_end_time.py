# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messageapp', '0002_messageannounce_end_time'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='messageannounce',
            name='end_time',
        ),
    ]
