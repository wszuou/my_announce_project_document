# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messageapp', '0004_messageannounce_end_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messageannounce',
            name='message',
            field=models.CharField(max_length=200),
        ),
    ]
