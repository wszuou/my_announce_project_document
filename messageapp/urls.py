from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.post_message, name='post_message'),
	url(r'^posted/$', views.posted, name='posted'),
	url(r'^message.json$', views.update_message, name='update_message'),
	url(r'^page/message.json$', views.update_message, name='update_message_page'),
	url(r'^page/$', views.message_page, name='message_page'),
]