# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('newsapp', '0007_auto_20160711_1658'),
    ]

    operations = [
        migrations.CreateModel(
            name='PageList',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(primary_key=True, serialize=False, parent_link=True, to='cms.CMSPlugin', auto_created=True)),
                ('name', models.CharField(null=True, blank=True, default='untitled', max_length=50)),
                ('interval', models.DurationField(null=True, blank=True, default=None)),
                ('index', models.IntegerField(null=True, blank=True, default=None)),
                ('page', models.OneToOneField(default=None, to='cms.Page', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.DeleteModel(
            name='Dummy',
        ),
        migrations.RemoveField(
            model_name='newspage',
            name='page',
        ),
        migrations.AddField(
            model_name='pagelist',
            name='program',
            field=models.ForeignKey(default=None, null=True, to='newsapp.NewsPage', blank=True),
        ),
    ]
