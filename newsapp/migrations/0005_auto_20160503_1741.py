# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0004_newspage_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='newspage',
            name='end_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='newspage',
            name='name',
            field=models.CharField(max_length=50, null=True, blank=True, default=None),
        ),
        migrations.AddField(
            model_name='newspage',
            name='publish',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='newspage',
            name='start_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='newspage',
            name='screen',
            field=models.CharField(max_length=50, null=True, blank=True, default=None),
        ),
    ]
