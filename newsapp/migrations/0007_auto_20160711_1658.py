# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import newsapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0006_auto_20160517_1659'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dummy',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                
            ],
        ),
        migrations.AlterField(
            model_name='newspage',
            name='name',
            field=models.CharField(null=True, default='untitled', blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='screen',
            name='name',
            field=models.CharField(null=True, default='untitled', blank=True, max_length=50),
        ),
    ]
