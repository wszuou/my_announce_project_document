# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('newsapp', '0003_remove_newspage_page'),
    ]

    operations = [
        migrations.AddField(
            model_name='newspage',
            name='page',
            field=models.ForeignKey(to='cms.Page', default=None, blank=True, null=True),
        ),
    ]
