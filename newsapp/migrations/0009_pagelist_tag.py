# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0008_auto_20160714_1452'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagelist',
            name='tag',
            field=models.CharField(default=None, blank=True, null=True, max_length=50),
        ),
    ]
