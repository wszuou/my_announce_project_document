# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0009_pagelist_tag'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagelist',
            name='picture',
            field=models.ImageField(null=True, upload_to='media/page_preview', blank=True),
        ),
    ]
