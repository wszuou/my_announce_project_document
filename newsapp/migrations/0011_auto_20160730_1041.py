# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0010_pagelist_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagelist',
            name='interval',
            field=models.DurationField(blank=True, null=True, default=datetime.timedelta(0, 30)),
        ),
    ]
