import unittest
from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from .models import NewsPage, Screen, PageList
from viewer.models import UserViewer
from cms.models.pagemodel import Page
from cms.api import *

from django.utils import timezone
from django.utils.timezone import localtime
import datetime
from django.test.utils import setup_test_environment
from django.contrib.auth import authenticate, login
from filer.models import Image, File
from django.contrib.auth.models import Permission, User
from time import sleep

setup_test_environment()
class Check_url_create_news_feature(unittest.TestCase):
	def setUp(self):
		self.client = Client()

	def test_create_page(self):
		response = self.client.get('/newsapp/new/')
		self.assertEqual(response.status_code, 302)

	def test_pagemodal_page(self): # create new page that set only title.
		response = self.client.get('/newsapp/new_page/')
		self.assertEqual(response.status_code, 302)

class manage_news_test(unittest.TestCase):
	def setUp(self):
		program1 = NewsPage.objects.create(name='program1')
		program2 = NewsPage.objects.create(name='program2')
		user = User.objects.create(username='boss', password='1234')

		#user_viewer = UserViewer.objects.create(user= user)
		#permission = Permission.objects.get(codename='permission_admin') # get permission
		#user_viewer.user_permissions.add(permission) # add permission
		#user_viewer.save()
		# create first text news
		news_text = PageList.objects.create(name="news1", program=program1)
		web_page = create_page('web_page1', 'news.html', 'en')
		web_page.publish('en')
		news_text.page = web_page
		page = news_text.page
		placeholder = page.placeholders.get(slot='content')
		add_plugin(placeholder, 'TextPlugin', 'en', body="<p>hello world</p>")
		page.publish('en')
		page.save()
		news_text.save()
		# create second text news
		news_text2 = PageList.objects.create(name="news2", program=program2)
		news_text2.prototype = news_text.id
		web_page = create_page('web_page2', 'news.html', 'en')
		web_page.publish('en')
		news_text2.page = web_page
		page = news_text2.page
		placeholder = page.placeholders.get(slot='content')
		add_plugin(placeholder, 'TextPlugin', 'en', body="<p>hello world</p>")
		page.publish('en')
		page.save()
		news_text2.save()
		# create first image news
		news_image = PageList.objects.create(name="news3", program=program1)
		web_page = create_page('web_page3', 'news.html', 'en')
		web_page.publish('en')
		news_image.page = web_page
		page = news_image.page
		placeholder = page.placeholders.get(slot='content')
		image_file = Image.objects.create(owner=user, 
											original_filename='img_test1',
											file= "/file_test/Slide8.PNG/")
		add_plugin(placeholder, 'FullImagePlugin', 'en', file=image_file)
		page.publish('en')
		page.save()
		news_image.save()
		# create seconds image news
		news_image2 = PageList.objects.create(name="news4", program=program2)
		news_image2.prototype = news_image.id
		web_page = create_page('web_page4', 'news.html', 'en')
		web_page.publish('en')
		news_image2.page = web_page
		page = news_image2.page
		placeholder = page.placeholders.get(slot='content')
		image_file = Image.objects.create(owner=user, 
											original_filename='img_test2',
											file= "/file_test/Slide8.PNG/")
		add_plugin(placeholder, 'FullImagePlugin', 'en', file=image_file)
		page.publish('en')
		page.save()
		news_image2.save()
		# create first pdf news
		news_pdf = PageList.objects.create(name="news5", program=program1)
		web_page = create_page('web_page5', 'news.html', 'en')
		web_page.publish('en')
		news_pdf.page = web_page
		page = news_pdf.page
		placeholder = page.placeholders.get(slot='content')
		pdf_file = File.objects.create(owner=user, 
											original_filename='pdf_test1',
											file= "/file_test/pdffrotest.pdf")
		add_plugin(placeholder, 'NewsPluginPDF', 'en', file=pdf_file)
		page.publish('en')
		page.save()
		news_pdf.save()
		# create second pdf news
		news_pdf2 = PageList.objects.create(name="news6", program=program2)
		news_pdf2.prototype = news_pdf.id
		web_page = create_page('web_page6', 'news.html', 'en')
		web_page.publish('en')
		news_pdf2.page = web_page
		page = news_pdf2.page
		placeholder = page.placeholders.get(slot='content')
		pdf_file = File.objects.create(owner=user, 
											original_filename='pdf_test2',
											file= "/file_test/pdffrotest.pdf")
		add_plugin(placeholder, 'NewsPluginPDF', 'en', file=pdf_file)
		page.publish('en')
		page.save()
		news_pdf2.save()
		# create first video news
		news_video = PageList.objects.create(name="news7", program=program1)
		web_page = create_page('web_page7', 'news.html', 'en')
		web_page.publish('en')
		news_video.page = web_page
		page = news_video.page
		placeholder = page.placeholders.get(slot='content')
		video_file = File.objects.create(owner=user, 
											original_filename='video_test1',
											file= "/file_test/test_video.mp4")
		add_plugin(placeholder, 'VideoPluginPublisher', 'en', file=video_file, title='video_file_test1', width=1024, height=768)
		page.publish('en')
		page.save()
		news_video.save()
		# create seconds video news
		news_video2 = PageList.objects.create(name="news8", program=program2)
		news_video2.prototype = news_video.id
		web_page = create_page('web_page8', 'news.html', 'en')
		web_page.publish('en')
		news_video2.page = web_page
		page = news_video2.page
		placeholder = page.placeholders.get(slot='content')
		video_file = File.objects.create(owner=user, 
											original_filename='video_test2',
											file= "/file_test/test_video.mp4")
		add_plugin(placeholder, 'VideoPluginPublisher', 'en', file=video_file, title='video_file_test2', width=1024, height=768)
		page.publish('en')
		page.save()
		news_video2.save()

		self.client = Client()
		self.client.login(username='boss', password='1234')

	def test_search_no_filter(self):

		response = self.client.get('/en/newsapp/search_news/', {'begin_time': '', 'search_name': '', 
							'search_screen': '', 'end_date': '', 'tag4': False, 'end_time': '',
							'begin_date': '', 'tag2': False, 'tag1': False, 'tag3': False})
		self.assertEqual(response.status_code, 200)
		print("all_page_id: ")
		print(response.context['all_page_id'].split('a'))
		self.assertEqual(len(response.context['all_page_id'].split('a')), 4)

"""
class Create_page_and_then_create_newspage(unittest.TestCase):
	def test_create_instance_page_and_newspage(self):
		screen1 = Screen.objects.create(name = 'screen1')
		create_page(title='my boo', template='INHERIT', language='en', published=True)
		my_news = NewsPage(name='boo page', screen=screen1, page='my boo', published='True')
		my_news.save()

		self.assertEqual(my_news.page, "my boo")

	def test_create_page_and_two_newspage_use_same_page(self):
		screen1 = Screen.objects.create(name = 'screen1')
		create_page(title='my boo', template='INHERIT', language='en', published=True)
		my_news = NewsPage(name='boo page', screen='screen1', page='my boo', published='True')		
		my_news.save()
		her_news = NewsPage(name='bee page', screen='screen1', page='my boo', published='True')
		her_news.save()

		self.assertEqual(my_news.screen, my_news.screen)
		self.assertEqual(my_news.page, "my boo")
		self.assertEqual(her_news.page, "my boo")
		self.assertEqual(my_news.page, her_news.page)

	def test_create_page_and_two_newspage_use_same_page_but_differ_screen(self):
		screen1 = Screen.objects.create(name = 'screen1')
		screen2 = Screen.objects.create(name = 'screen2')
		create_page(title='my boo', template='INHERIT', language='en', published=True)
		my_news = NewsPage(name='boo page', screen=screen1, page='my boo', published='True')		
		my_news.save()
		her_news = NewsPage(name='bee page', screen=screen2, page='my boo', published='True')
		her_news.save()

		self.assertEqual(my_news.page, "my boo")
		self.assertEqual(her_news.page, "my boo")
		self.assertEqual(my_news.page, her_news.page)
		self.assertNotEqual(my_news.screen, her_news.screen)

	def test_create_page_and_two_newspage_use_differ_page_but_same_screen(self):
		screen2 = Screen.objects.create(name = 'screen2')
		create_page(title='my boo', template='INHERIT', language='en', published=True)
		create_page(title='my bee', template='INHERIT', language='en', published=True)
		my_news = NewsPage(name='boo page', screen=screen2, page='my boo', published='True')		
		my_news.save()
		her_news = NewsPage(name='bee page', screen=screen2, page='my bee', published='True')
		her_news.save()

		self.assertEqual(my_news.screen, my_news.screen)
		self.assertEqual(my_news.page, "my boo")
		self.assertEqual(her_news.page, "my bee")
		self.assertNotEqual(my_news.page, her_news.page)

	def test_create_page_and_two_newspage_use_differ_page_but_differ_screen(self):
		screen1 = Screen.objects.create(name = 'screen1')
		screen2 = Screen.objects.create(name = 'screen2')	
		create_page(title='my boo', template='INHERIT', language='en', published=True)
		create_page(title='my bee', template='INHERIT', language='en', published=True)
		my_news = NewsPage(name='boo page', screen='screen1', page='my boo', published='True')		
		my_news.save()
		her_news = NewsPage(name='bee page', screen='screen2', page='my bee', published='True')
		her_news.save()

		self.assertEqual(my_news.page, "my boo")
		self.assertEqual(her_news.page, "my bee")
		self.assertNotEqual(my_news.page, her_news.page)

		self.assertEqual(my_news.screen, "screen1")
		self.assertEqual(her_news.screen, "screen2")
		self.assertNotEqual(my_news.screen, her_news.screen)

class RouteCreateNewsTest(LiveServerTestCase): 
	# test button clicking that bring user to page accord below list
	# crate.html --> pagemodal.html --> [target page that has empty placeholder.]
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)

	def tearDown(self):
		self.browser.quit()

	def test_visit_Status_screen_page(self):
		self.browser.get(self.live_server_url + '/')
		heading = self.browser.find_element_by_tag_name('h1')
		self.assertEqual(heading.text, 'Status screen page')

	def test_visit_Programs_page(self):
		self.browser.get(self.live_server_url + '/programs')
		heading = self.browser.find_element_by_tag_name('h1')
		self.assertEqual(heading.text, 'Programs page')

	def test_visit_Screens_page(self):
		self.browser.get(self.live_server_url + '/screens')
		heading = self.browser.find_element_by_tag_name('h1')
		self.assertEqual(heading.text, 'Screens page')

	def tet_visit_Time_table_page(self):
		self.browser.get(self.live_server_url + '/time_table')
		heading = self.browser.find_element_by_tag_name('h1')
		self.assertEqual(heading.text, 'Time table page')

	def test_create_single_announce(self):
		self.browser.get(self.live_server_url + '/')

		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('NEW', body.text)
		browser.find_element_by_name("NEW_button").click()
		name_field = self.browser.find_element_by_name('name_field')
		name_field.send_keys('News boo')

		screen_field = self.browser.find_element_by_name('screen_field')
		screen_field.send_keys('screen1')

		start_date_field = self.browser.find_element_by_name('start_date_field')
		start_date_field.send_keys('10/4/2016')
		start_time_field = self.browser.find_element_by_name('start_time_field')
		start_time_field.send_keys('9:15')

		end_date_field = self.browser.find_element_by_name('end_date_field')
		end_date_field.send_keys('10/4/2016')
		end_time_field = self.browser.find_element_by_name('end_time_field')
		end_time_field.send_keys('10:15')

		browser.find_element_by_name("save_button").click()

	def test_create_announce_but_cancel(self):
		self.browser.get(self.live_server_url + '/')
		browser.find_element_by_name("NEW_button").click()
		heading = self.browser.find_element_by_tag_name('h1')
		self.assertEqual(heading.text, 'Create page')
		browser.find_element_by_name("close_button").click()
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Programs page\n', body.text)
"""