#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.utils.timezone import localtime 
import json
from datetime import datetime, date, time, timedelta
import datetime, pytz

from .models import NewsPage, Screen, PageList
from .forms import ImagefileForm, PDFfileForm, VideofileForm, SearchNewsForm, VideoURLForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from cms.models.pagemodel import Page

from viewer.models import UserViewer
from django.contrib.auth.decorators import permission_required, login_required, user_passes_test
from django.utils.decorators import method_decorator

NEWSAPP_ERROR_CHOICES = [
	'This name is used already.',
	'start_time is repeatedly with anathor.',
	'end_time is repeatedly with anathor.',
	'start_time is more than end_time.',
	'start_time is equal end_time.'
]

@permission_required('viewer.permission_admin')
@login_required
def show_all_screen(request):
	""" show a big picture that what program is showed in each screen. 
	but for simple show only status on or off.(still connect with server)"""
	template_name = 'newsapp/status_screen.html'
	context = {}
	#context['screens'] = get_list_screen
	program_list = []
	for screen in get_list_screen():
		context_p = {}
		programs = NewsPage.objects.filter(screen=screen, publish=True)
		if len(programs) > 1: # if find more than 1 publish program
			for program in programs: # find only 1 publish program
				if program.was_published(): # check if be publish.
					context_p['program'] = program
					if PageList.objects.filter(program=program):
						context_p['page'] = PageList.objects.filter(program=program)[0] # get only first page for show picture.
					else:
						context_p['page'] = False
					break # stop this loop
				else:
					context_p['program'] = False
					context_p['page'] = False
		elif len(programs) == 1: # find 1 publish program
			if programs[0].was_published():
				context_p['program'] = programs[0] 
				if PageList.objects.filter(program=programs[0]):
					context_p['page'] = PageList.objects.filter(program=programs[0])[0] # get only first page for show picture.
				else:
					context_p['page'] = False
			else:
				context_p['program'] = False
				context_p['page'] = False
		else: # if no have publish program
			context_p['program'] = False
			context_p['page'] = False
		context_p['screen'] = screen
		program_list.append(context_p)
	context['data_screen'] = program_list
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def get_all_announce(request):
	""" get list of announce."""
	template_name = 'newsapp/programs.html'
	all_news = []
	all_screen  = get_list_screen()
	for news in get_list_news():
		all_news.append(news)
	return render(request, template_name, {'all_news': all_news, 'all_screen': all_screen})
# temperal chanal to create page
@permission_required('viewer.permission_admin')
@login_required
def gotocreatepage(request):
	return render(request, 'newsapp/create.html', {})

@permission_required('viewer.permission_admin')
@login_required
def to_create_announce(request, news_id = None, error_message=None): 
	"""if no news_id send data to choose in create page.
	   if get news_id send date to choose in create page and send old data 
	to fill in each field form.
	"""
	from cms.api import create_page # use cms api
	from django.core.files import File
	template_name = 'newsapp/create.html'
	screens = get_list_screen() # send screens list for selection
	
	if news_id == None:
		new_program = NewsPage() # create new program
		first_page = PageList()  # create first page for new program
		new_program.save()
		first_page.program = new_program # first is sub page of new_program
		first_page.index = 0  # first page has index 0.
		first_page.save()

		web_page = create_page('web_page' + str(first_page.id), 'news.html', 'en') # create page
		web_page.publish('en')
		first_page.page = web_page # connect real page with first_page
		first_page.url = web_page.get_public_url('en')  + '?edit_off'
		first_page.save()
		first_page.name = 'page'+str(first_page.id)
		first_page.save()
		
		filename = 'empty_page.png'	# file name
		filepath = 'media/media/page_preview/empty_page.png'    # file path 
		with open(filepath, 'rb') as f:	# create empty page to be preview page 
			file_obj = File(f, name=filename)
			first_page.picture.save(str(first_page.name) + '.png', file_obj) # rename file following by PageList.name and set PageList.preview
			first_page.save()

		context = {'program':new_program, 'list_page':[first_page], 'all_screen':screens} # list_page will be list because forloop useing in create.html
		return HttpResponseRedirect(reverse('newsapp:to_create_announce', 
				kwargs= {'news_id':new_program.id} ))
	else: # edit program then get data's program to fill in create.html
		program_geten = get_object_or_404(NewsPage ,pk=news_id)
		# code belog one line that was commented is old list_page.
		#list_page = PageList.objects.filter(program=program_geten).order_by('index') # get all PageList in this program and sort them by index.
		# start check has_edit
		from djangocms_text_ckeditor.models import Text
		list_page = []
		pages = PageList.objects.filter(program=program_geten).order_by('index') # get all page
		for web_page in pages:
			placeholders = list(web_page.page.get_placeholders())
			if placeholders[0].get_plugins_list():
				plugins = placeholders[0].get_plugins_list()[0]
				instance, plugin = plugins.get_plugin_instance() # get plugin class
				if type(instance) is Text: # if this page has Text plugin
					list_page.append({'web_page':web_page, 'has_edit':True}) # set has edit = True
				else: # if not be Text plugin
					list_page.append({'web_page':web_page, 'has_edit':False}) # set has edit = False
			else:
				list_page.append({'web_page':web_page, 'has_edit':False}) # set has edit = False
		# end check has_edit
		context = {'program':program_geten, 
				   'list_page':list_page, 
				   'all_screen':screens,
				   'news_screen':program_geten.screen.name if program_geten.screen != None else '',
				   'news_start_date':program_geten.start_time.strftime("%d-%m-%Y") if program_geten.start_time != None else '', 
				   'news_start_time':( program_geten.start_time+datetime.timedelta(hours=7) ).strftime("%H:%M") if program_geten.start_time != None else '',
				   #'news_end_date':program_geten.end_time.strftime("%d-%m-%Y") if program_geten.end_time != None else '',
				   'news_end_time':(program_geten.end_time+datetime.timedelta(hours=7) ).strftime("%H:%M") if program_geten.end_time != None else '',
				   }
	
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def new_announce(request, news_id = None):
	""" create new announce.
	send error message in those case.
	1. name is repeatedly with anathor.
	2. start_time and end_time are repeatedly with anathor.
	3. send start_time or end_time back to page for warn that these time
	are not suitable.
	"""
	from datetime import datetime, date, time
	if news_id == None:
		news_page = NewsPage()
	else:
		news_page = get_object_or_404(NewsPage, pk=news_id)
	start_datetime, end_datetime = '', '' # use for send to check error.
	if request.method == 'POST':
		if request.POST['announce_name']:
			news_page.name = request.POST['announce_name']
		if request.POST['screen'] and request.POST['screen'] != "":
			screen_geten = Screen.objects.get(name = request.POST['screen'])
			news_page.screen = screen_geten
		if request.POST['news_page']:
			page_geten = request.POST['news_page']
			target_page = news_page.my_get_title_page(page_geten)
			news_page.page = target_page
		if request.POST['announce_start_date'] and request.POST['announce_start_time']:
			start_date = request.POST['announce_start_date'].split('-') # 
			start_date = [ int(i) for i in start_date ] # convert to int list
			start_time = request.POST['announce_start_time'].split(':')
			start_time = [ int(i) for i in start_time ] # convert to int list
			
			start_date = date(start_date[2], start_date[1], start_date[0])
			start_time = time(start_time[0], start_time[1])
			start_datetime = datetime.combine(start_date, start_time)
			news_page.start_time = start_datetime
		if request.POST['announce_end_date'] and request.POST['announce_end_time']:
			end_date = request.POST['announce_end_date'].split('-') # 
			end_date = [ int(i) for i in end_date ] # convert to int list
			end_time = request.POST['announce_end_time'].split(':')
			end_time = [ int(i) for i in end_time ] # convert to int list

			end_date = date(end_date[2], end_date[1], end_date[0])
			end_time = time(end_time[0], end_time[1])
			end_datetime = datetime.combine(end_date, end_time)
			news_page.end_time = end_datetime
		# NewsPage's name should not repeatedly with any NewsPage object.
		# start_time and end_time should not repeatedly with anathor NewsPage object.
		# send error messsage if screen name is repeadly with anathor.
		context = check_error_announce(request.POST['announce_name'], start_datetime, end_datetime, request.POST['screen'], news_id)
		if context != 0:
			return HttpResponseRedirect(reverse('newsapp:err_create_announce', 
				kwargs=context))	
		news_page.save()
	return HttpResponseRedirect(reverse('newsapp:get_all_announce'))

def check_error_announce(announce_name, start_datetime, end_datetime, screen_name, news_id = None ):
	utc = pytz.utc # get tzinfo UTC
	start_datetime = start_datetime.replace(tzinfo=utc) - timedelta(hours=7) # set tzinfo to be UTC
	end_datetime = end_datetime.replace(tzinfo=utc) - timedelta(hours=7)
	print(start_datetime)
	print(end_datetime)
	err = []
	set_screen = Screen.objects.get(name=screen_name)
	print(set_screen)
	news_name = NewsPage.objects.filter(name=announce_name, screen=set_screen)
	if news_id != None:
		news_name = news_name.exclude(id = news_id)
		news_geten = get_object_or_404(NewsPage, pk=news_id)
	if len(news_name)>0:
		err.append('0') #this name is used already.
	print('check news id')
	if start_datetime != '':# and type(start_datetime) is datetime :
		news = NewsPage.objects.filter(start_time__lte = start_datetime, end_time__gte = start_datetime, screen=set_screen)
		print(news)
		if news_id != None:
			news = news.exclude(id = news_id)
		if len(news) > 0:
			err.append('1') #start_time is repeatedly with anathor.
			##################
			for new in news:
				err.append('id'+ str(new.id)) # add id news that repeated.
				###########
	print('check start_datetime')
	if end_datetime != '' :#and type(end_datetime) is datetime :
		news = NewsPage.objects.filter(start_time__lte = end_datetime, end_time__gt = end_datetime, screen=set_screen)
		print(news)
		if news_id != None:
			news = news.exclude(id = news_id)
		if len(news) > 0:
			err.append('2') #end_time is repeatedly with anathor
			##################
			for new in news:
				err.append('id'+ str(new.id)) # add id news that repeated.
				###########
	print('check end_datetime')
	if ( start_datetime != '' ) and ( end_datetime != '' ):
		if end_datetime < start_datetime :
			err.append('3')
		elif end_datetime == start_datetime :
			err.append('4')
	print('check start_datetime > end_datetime')
	err_set = 'a'.join(err) # insert a to each error message because this data is sended by urls.
	print(err_set)
	if len(err) > 0:
		if news_id == None:
			return {'error_message':err_set}
		else:
			return { 'news_id':news_id, 'error_message':err_set}
	else: # no error
		return 0

@permission_required('viewer.permission_admin')
@login_required
def get_announce_data(request, news_id):
	""" send old data to fill in form before editing. """
	target_page =get_object_or_404(NewsPage, pk=news_id)
	template_name = 'newsapp/create.html'
	pages = []
	for each_page in get_list_page():
		pages.append(each_page.get_title_obj('en').title)
	context = {'all_page':pages,'screen':target_page.screen, 'target_page':target_page.page.get_title_obj('en').title} 
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def delete_announce(request, news_id): #delete program
	new_page = get_object_or_404(NewsPage, pk=news_id)
	pages = PageList.objects.filter(program= new_page)
	if len(pages) >0:
		for page in pages:
			childs = PageList.objects.filter(prototype=int(page.id))
			for child in childs:
				child.prototype = None
				child.save()
			if page.page: # if have page in this PageList
				
				page.picture.delete() # delete thumnail picture
				placeholders = list(page.page.get_placeholders())
				place = placeholders[0]
				plugins = place.get_plugins_list()
				if len(plugins) != 0: # if has plugin
					plug = plugins[0]
					instance, plugin = plug.get_plugin_instance()
					if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
						instance.delete()
					elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
						if instance.file != None:
							instance.file.delete()  # delete file plugin
					else: # if be image plugin or pdf plugin
						instance.file.delete()  # delete file plugin

				page.page.delete() # delete real page that connect with this PageList.
			page.delete()
	new_page.delete()
	return HttpResponseRedirect(reverse('newsapp:get_all_announce'))

def screen_announce(request, screen_text, time_text=None):
	""" play announce page refer time table of each announce.
	1. check if published
	2. if not published change to other news
	3. if published continue show present news
	"""
	from cms.views import details
	from django.template import Context, Template
	from datetime import datetime, date, time, timedelta
	template_name = "newsapp/show.html"
	context = {}
	context['target_url'] = datetime.now().strftime("%H%M%S")
	context['screen_name'] = screen_text
	screen_geten = Screen.objects.get(name = screen_text)
	context['screen_sound'] = screen_geten.mute
	# get only one publish page else show kmutnb synbol.

	news_geten = NewsPage.objects.filter(screen = screen_geten, start_time__isnull = False, end_time__isnull = False)#, publish = True)
	print("length news_geten: "+ str(len(news_geten)))
	meet_news =  False
	if len(news_geten) > 0:
		for pub_news in news_geten:
			if pub_news.was_published() and pub_news.is_completed():
				# develop This part.
				print(">>>>>>>>>>>>>>>>>>>>>>> meet_news")
				page_list = PageList.objects.filter(program = pub_news).order_by('index') # get all page in this program.
				if len(page_list) != 0:
					meet_news = True
					start_time = pub_news.start_time
					start_time = datetime.combine(date(start_time.year, start_time.month, start_time.day) , time(start_time.hour+7, start_time.minute) ) 
					end_time = pub_news.end_time
					end_time = datetime.combine(date(end_time.year, end_time.month, end_time.day) , time(end_time.hour+7, end_time.minute) ) 
					now_time = datetime.now()  # bring out a timezone.
					all_time = 0
					for sub_page in page_list: # get sumation of time in all page
						all_time = all_time + sub_page.interval.seconds 
					if all_time == 0: # must be 30 seconds for  at least.
						all_time = 30
					print(now_time)
					print('all_time: '+ str(all_time))
					round_num = ( (end_time - start_time).seconds/all_time )+1 # all_time is integer.
					mark_time = start_time
					if round_num > 0: # if must play this program more than a time.
						for i in range(int(round_num)):
							if all_time * (i+1) > (now_time - start_time).seconds:
								mark_time = start_time + timedelta(seconds=(all_time * i))
								print("round geten: " + str(i))
								print("mark_time geten: "+ str(mark_time))
								break

					for sub_page in page_list:
						if mark_time + sub_page.interval > now_time:
							# bring this page to show screen
							template = details(request, sub_page.page.get_slug('en'))
							if mark_time + sub_page.interval > end_time: # if refresh time more than end time.
								context['refresh_time'] = (end_time - now_time).seconds
							else:
								context['refresh_time'] = (mark_time + sub_page.interval - now_time).seconds
							print("====================  news index :"+str(sub_page.index))
							break
						else:
							mark_time = mark_time + sub_page.interval
							
					#template = details(request, pub_news.page.get_slug('en'))
					#context['refresh_time'] = pub_news.get_refresh_time()
					#context['news_page_title'] = pub_news.page.get_title_obj('en')
					break

		if not meet_news: 
			last_news = PageList.objects.get(name = "kmutnb_symbol")
			template = details(request, last_news.page.get_slug('en'))
			context['refresh_time'] = 10
			context['symbol_page'] = 'is_symbol_page'
	else:
		last_news = PageList.objects.get(name = "kmutnb_symbol")
		template = details(request, last_news.page.get_slug('en'))
		context['refresh_time'] = 10
		context['symbol_page'] = 'is_symbol_page'

	content = ''

	if hasattr(template, 'render'):
    	# TemplateResponse must call render() before we can get the content
		content = template.render().content
	else:
    	# HttpResponse does not have a render() method
		content = template.content
	t = Template(content)
	c = Context({})
	context['newsapp_page_content'] = t.render(c)
	
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def get_time_table(request, screen_id):
	""" show time table of program for each screen"""
	template_name = "newsapp/table.html"
	context = {}
	list_news= []
	screen_obj = get_object_or_404(Screen, pk=screen_id)
	list_program = NewsPage.objects.filter(screen=screen_obj).order_by('start_time')
	for each_program in list_program: 
		list_news.append(each_program)
	context['table_news'] = list_news
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def show_example(request, page_id): # may be no use
	""" show example of announce when It's played in web page."""
	from cms.api import add_plugin, create_page
	from cms.models.placeholdermodel import Placeholder
	from cms.views import details
	from django.template import Context, Template
	template_name = "newsapp/example.html"
	context = {}
	page = get_object_or_404(Page, pk=page_id)
	#placeholder = page.placeholders.get(slot='content')
	#add_plugin(placeholder, 'TextPlugin', 'en', body='hello world')
	template = details(request, page.get_slug('en'))
    #return render(request, template_name, context)

	content = ''
	if hasattr(template, 'render'):
    	# TemplateResponse must call render() before we can get the content
		content = template.render().content
	else:
    	# HttpResponse does not have a render() method
		content = template.content
	t = Template(content)
	c = Context({})
	context['example_content'] = t.render(c)
	context['example_mark'] = 'example-page-mark'
	return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def delete_page(request, page_id): # may be no use
	page_geten = get_object_or_404(Page, pk=page_id)
	page_geten.delete()
	return HttpResponseRedirect(reverse('newsapp:show_all_page'))

@permission_required('viewer.permission_admin')
@login_required
def create_announce_page(request): # # may be no use  .next step, automate add messageapp and timecontroller.
	from cms.api import create_page
	if request.method == 'POST':
		if request.POST['page_title']:
			new_page = create_page(title=request.POST['page_title'], template='INHERIT', language='en', published=True)
	return redirect(new_page.get_draft_url('en'))
	#return HttpResponseRedirect(reverse('newsapp:show_all_screen'))

def get_list_screen():
	return Screen.objects.all()

def get_list_page():
	return Page.objects.all()

def get_list_news():
	return NewsPage.objects.all()

@permission_required('viewer.permission_admin')
@login_required
def to_create_screen(request, screen_id = None, error_message=None): # path to create screen page
	template_name = "newsapp/screen/create_screen.html"
	context = {}
	context['error_message'] = warning_error(error_message) if error_message != None else ''
	if screen_id == None:
		return render(request, template_name, context)
	else:
		screen_obj = get_object_or_404(Screen, pk=screen_id)
		context['screen_name'] = screen_obj.name
		context['mute'] = screen_obj.mute
		context['screen_obj'] = screen_obj
		return render(request, template_name, context)

@permission_required('viewer.permission_admin')
@login_required
def create_screen(request, screen_id = None):
	if screen_id == None : # new screent
		new_screen = Screen() 
	else: # edit screen
		new_screen = get_object_or_404(Screen, pk=screen_id)
	if request.method == 'POST':
		if request.POST['screen_name']:
			new_screen.name = request.POST['screen_name']
			# send error messsage if screen name is repeadly with anathor.
			screen_geten = Screen.objects.filter(name=request.POST['screen_name'])
			if screen_id != None:
				screen_geten = screen_geten.exclude(name = new_screen.name)
			if len(screen_geten)>0:
				if screen_id == None:
					return HttpResponseRedirect(reverse('newsapp:err_create_screen', 
						kwargs={ 'error_message':'0'}))
				else:
					return HttpResponseRedirect(reverse('newsapp:err_create_screen', 
						kwargs={ 'screen_id':new_screen.id, 'error_message':'0'}))
		try:
			if request.POST['screen_mute']:
				new_screen.mute = True
		except Exception: # MultiValueDictKeyError
			new_screen.mute = False
		new_screen.save()
	return HttpResponseRedirect(reverse('newsapp:view_screen'))	

@permission_required('viewer.permission_admin')
@login_required
def view_screen(request):
	template_name = "newsapp/screen/view_screen.html"
	return render(request, template_name, {'screen_list':get_list_screen})

@permission_required('viewer.permission_admin')
@login_required
def delete_screen(request, screen_id):
	new_screen = get_object_or_404(Screen, pk=screen_id)
	programs = NewsPage.objects.filter(screen = new_screen) # get NewsPage that relate with this screen.
	if len(programs) > 0: # if have NewsPage that relate with this screen
		for program in programs: 
			program.screen = None # bring this screen out from each NewsPage
			program.save()
	new_screen.delete()
	return HttpResponseRedirect(reverse('newsapp:view_screen'))

def warning_error(error_message):
	messages = error_message.split('a')
	return [ NEWSAPP_ERROR_CHOICES[int(message)] for message in messages ]

@permission_required('viewer.permission_admin')
@login_required
def edit_program(request): # work while click save, publish or update.
	client_data = json.loads(request.GET.get('news_data'))
	client_data = json.loads(client_data) # convert to dict
	from datetime import datetime, date, time, timedelta
	program = get_object_or_404(NewsPage, pk= int(client_data["Program"][0]["id"])) # get program instance by id.
	if client_data["button"] == "publish_button": # if click publish button.
		program.publish = True
	
	program_info = client_data["Program"][0] # dict that collect progarm data.
	if program_info["name"] != "":
		program.name = program_info["name"] # save name
	
	if program_info["screen"] != "":
		program.screen = Screen.objects.get(name=program_info["screen"]) # save screen
	
	if program_info["start_datetime"] != "": # save start_time
		start_data = program_info["start_datetime"].split('&')
		start_date = start_data[0].split('-')
		start_date = [ int(i) for i in start_date ] # convert to int list
		start_time = start_data[1].split(':')
		start_time = [ int(i) for i in start_time ]
		
		start_date = date(start_date[2], start_date[1], start_date[0]) # create date by year month date 
		start_time = time(start_time[0], start_time[1]) # create time by hours and minute.
		start_datetime = datetime.combine(start_date, start_time) # create datetime
		program.start_time = start_datetime
	
	if program_info["end_datetime"] != "": # save end_time
		end_data = program_info["end_datetime"].split('&')
		end_date = end_data[0].split('-')
		end_date = [ int(i) for i in end_date ]
		end_time = end_data[1].split(':')
		end_time = [ int(i) for i in end_time ]
		
		end_date = date(end_date[2], end_date[1], end_date[0])	
		end_time = time(end_time[0], end_time[1]) 			
		end_datetime = datetime.combine(end_date, end_time)
		program.end_time = end_datetime
	
	pages_info = client_data["PageList"]
	for each_page in pages_info:
		page = get_object_or_404(PageList, pk=int(each_page["id"])) 
		if each_page["interval"] != "":
			duration = each_page["interval"].split(":")
			if duration[0] == '0' and duration[1] == '00' and duration[2] == '00':
				page.interval = timedelta(seconds=30) # set time interval
			else:
				page.interval = timedelta(hours=int(duration[0]), minutes=int(duration[1]), seconds=int(duration[2])) # set time interval
		if each_page["index"] != "":
			page.index = int(each_page["index"])
		if each_page["tag"] != "":
			page.tag = each_page["tag"]
		else:
			page.tag = "None"
		page.save()

	context = check_error_announce(program_info["name"], start_datetime, end_datetime, program_info["screen"], int(client_data["Program"][0]["id"])) # check error

	if context != 0 : # if have some error.
		if len(context) != 0:
			error_message = ""
			errors = context['error_message'].split('a') 
			for error in errors:
				if error[:2] == 'id':
					re_news = get_object_or_404(NewsPage, pk=int(error[2:]))
					start_time = str(re_news.start_time.day)+'/'+str(re_news.start_time.month)+'/'+str(re_news.start_time.year) + ' time ' + (re_news.start_time+timedelta(hours=7)).strftime('%H:%M')+ '. ' 
					end_time = str(re_news.end_time.day)+'/'+str(re_news.end_time.month)+'/'+str(re_news.end_time.year) + ' time ' + (re_news.end_time+timedelta(hours=7)).strftime('%H:%M')+ '. '
					error_message = error_message + '  time repeat with ' + re_news.name + ' interval: ' + start_time + ' to ' + end_time + '  '
				else:
					error_message = error_message + NEWSAPP_ERROR_CHOICES[int(error)] + ' '
			#for error in context['error_message']: # get error message
			#	error_message = error_message + NEWSAPP_ERROR_CHOICES[int(error)]
			server_data = {"message": "error", "error_text":error_message}
	else:
		program.save()
		server_data = {"message":"success", "button":client_data["button"]}
	# group the buuton that click by save, update and publish has anathor method after save data.
	#server_data = {"message":"success", "button":"save_button"}
	return JsonResponse(server_data)

@permission_required('viewer.permission_admin')
@login_required
def page_to_draft(request): # change program to draft
	client_data = json.loads(request.GET.get('newspage_data'))
	news_page = get_object_or_404(NewsPage, pk=int(client_data['program_id']))
	news_page.publish = False
	news_page.save()
	return JsonResponse({"message":"success"})

@permission_required('viewer.permission_admin')
@login_required
def add_new_page(request):
	# create new page in create.html by click plus button.
	from cms.api import create_page
	from django.core.files import File
	client_data = json.loads(request.GET.get('newspage_data'))
	
	program_id = int(client_data["program_id"]) # get newspage id from client data
	program = get_object_or_404(NewsPage, pk=program_id) # call program that will be add new page.
	index_geten = len(PageList.objects.filter(program=program))# get length of page
	new_page = PageList() # create new PageList
	new_page.index = index_geten
	new_page.program = program
	new_page.save()
	new_page.name = 'page'+str(new_page.id) # set name of new_page
	new_page.save()

	web_page = create_page('web_page' + str(new_page.id), 'news.html', 'en') # create page
	web_page.publish('en')
	new_page.page = web_page # connect real page with new_page
	new_page.url = web_page.get_public_url('en')  + '?edit_off'
	new_page.save()

	filename = 'empty_page.png'	# file name
	filepath = 'media/media/page_preview/empty_page.png'    # file path 
	with open(filepath, 'rb') as f:	# create empty page to be preview page 
		file_obj = File(f, name=filename)
		new_page.picture.save(str(new_page.name) + '.png', file_obj) # rename file following by PageList.name and set PageList.preview
		new_page.save()

	server_data = {"server_data":str(client_data["program_id"]), "page_id":str(new_page.id),
					"page_interval":"0:00:30", "page_tag": "None", "page_url":str(new_page.url),
					"picture_url":str(new_page.picture.url)} # send back a new_page id.
	return JsonResponse(server_data) 

@permission_required('viewer.permission_admin')
@login_required
def delete_page_select(request):
	# delete page that was selected by click garbage button.
	client_data = json.loads(request.GET.get('newspage_delete'))
	print(client_data["page_id"])
	
	program_id = int(client_data["program_id"]) # get newspage id from client data
	program = get_object_or_404(NewsPage, pk=program_id) # call program that will be add new page.
	page_id = int(client_data["page_id"]) # get id page that will be delete.
	page_geten = get_object_or_404(PageList, pk=page_id) # get page that will be delete.		
	delete_index = page_geten.index
	#### start delete file part ####
	page_geten.picture.delete() # delete thumnail picture
	placeholders = list(page_geten.page.get_placeholders())
	place = placeholders[0]
	plugins = place.get_plugins_list()
	if len(plugins) != 0: # if has plugin
		plug = plugins[0]
		instance, plugin = plug.get_plugin_instance()
		if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
			instance.delete()
		elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
			if instance.file != None:
				instance.file.delete()  # delete file plugin
		else: # if be image plugin or pdf plugin
			instance.file.delete()  # delete file plugin
	#### end delete file part ####
	page_geten.page.delete()
	page_geten.delete() # delete page 

	pages = PageList.objects.filter(program=program).order_by('index') # get all page roder by index.
	if len(pages) > 0: # if number of page > 0 then sort their index again.
		for page in pages:
			if page.index > delete_index: 
				page.index = page.index - 1 # adjust index
				page.save()
	server_data = {'delete_index':str(delete_index)} # send delete_index back for delete page in create.html
	return JsonResponse(server_data) 

def get_preview_page(page_list, page, request): # screenshot page to create preview page.
	import selenium.webdriver
	from selenium.webdriver.common.by import By
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.support import expected_conditions as EC

	from django.core.files import File
	from filer.models import Image
	from pyvirtualdisplay import Display
	import os
	print(os.getcwd())

	display = Display(visible=0, size=(800, 600))
	display.start()

	#driver = selenium.webdriver.PhantomJS()#executable_path='/mysite/static/phantomjs/bin/phantomjs') # create webdriver and set path to phantomjs	
	driver = selenium.webdriver.Firefox()
	driver.set_window_size(1024, 768)	# set window size
	driver.get('http://'+str(request.get_host()) +page.get_public_url('en'))	# webdriver get url
	wait = WebDriverWait(driver, 10)	# wait webdriver
	driver.execute_script('document.body.style.background = "white"')	# set white background
	driver.save_screenshot('out.png')	# save screenshot 'out.png'
	try: # if webdriver time out then quit it.
		element = WebDriverWait(driver, 5).until(
			EC.presence_of_element_located((By.ID, "myDynamicElement"))
		)
	except Exception: # Timeout Exception
		driver.quit()
	else:
		driver.quit()
	display.stop()

	filename = 'out.png'	# file name
	filepath = 'out.png'    # file path 
	with open(filepath, 'rb') as f:	# open file 'out.png' 
		file_obj = File(f, name=filename)
		page_list.picture.delete()  # delete old picture
		page_list.picture.save(str(page_list.name) + '.png', file_obj) # rename file following by PageList.name and set PageList.preview
		page_list.save()

@permission_required('viewer.permission_admin')
@login_required
def add_text_plugin(request, page_id): # add text plugin to page that was selected.
	from cms.api import add_plugin, create_page
	page_list = get_object_or_404(PageList, pk=page_id)  # call PageList by id
	page = page_list.page  # call page by PageList above.
	if page_list.picture != None:  # if PageList have preview already mean that edit plugin then delete page and create new page
		
		page_list.picture.delete() # delete thumnail picture
		placeholders = list(page_list.page.get_placeholders())
		place = placeholders[0]
		plugins = place.get_plugins_list()
		if len(plugins) != 0: # if has plugin
			plug = plugins[0]
			instance, plugin = plug.get_plugin_instance()
			if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
				instance.delete()
			elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
				if instance.file != None:
					instance.file.delete()  # delete file plugin
			else: # if be image plugin or pdf plugin
				instance.file.delete()  # delete file plugin

		page.delete()
		web_page = create_page('web_page' + str(page_list.id), 'news.html', 'en') # create page
		page_list.page = web_page 
		page_list.prototype = None
		page_list.save()
		page = page_list.page
		
	if request.method == "POST": # pull data from text area (ckeditor).
		if request.POST['editor1']:
			placeholder = page.placeholders.get(slot='content')
			add_plugin(placeholder, 'TextPlugin', 'en', body=request.POST['editor1']) 	# add text plugin to page by set data from ckeditor.
			page.publish('en')
			page.save()
		
	get_preview_page(page_list, page, request) # get_preview_page()
	# set preview page of PageList
	return HttpResponseRedirect(reverse('newsapp:to_create_announce', 
		kwargs={'news_id':page_list.program.id}))	# back to create.html

@permission_required('viewer.permission_admin')
@login_required
def add_image_plugin(request, page_id): # add image plugin to page that was selected.
	from cms.api import add_plugin, create_page
	from django.core.files import File
	from filer.models import Image
	from django.contrib.auth.models import User
	page_list = get_object_or_404(PageList, pk=page_id)	# call PageList by id
	page = page_list.page	# call page by PageList above.
	
	if request.method == "POST": # pull data file from create.html
		MyImageForm = ImagefileForm(request.POST, request.FILES)
		user = User.objects.get(username='root')
		print(">>>>>>>>>>>  MyImageForm is valid: "+ str(MyImageForm.is_valid()) )
		if MyImageForm.is_valid():

			if page_list.picture != None:	# if PageList have preview already mean that edit plugin then delete page and create new page
				
				page_list.picture.delete() # delete thumnail picture
				placeholders = list(page_list.page.get_placeholders())
				place = placeholders[0]
				plugins = place.get_plugins_list()
				if len(plugins) != 0: # if has plugin
					plug = plugins[0]
					instance, plugin = plug.get_plugin_instance()
					if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
						instance.delete()
					elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
						if instance.file != None:
							instance.file.delete()  # delete file plugin
					else: # if be image plugin or pdf plugin
						instance.file.delete()  # delete file plugin

				page.delete()
				web_page = create_page('web_page' + str(page_list.id), 'news.html', 'en') # create page
				page_list.page = web_page
				page_list.prototype = None
				page_list.save()  
				page = page_list.page	

			image_file =  Image.objects.create(owner=user, 
												original_filename='img_'+ page_list.name,
												file= MyImageForm.cleaned_data["ImageUploader"] ) # get file uploaded
			placeholder = page.placeholders.get(slot='content')
			add_plugin(placeholder, 'FullImagePlugin', 'en', file=image_file) # add image plugin
			page.publish('en')
			page.save()

			get_preview_page(page_list, page, request) # get_preview_page()
	return HttpResponseRedirect(reverse('newsapp:to_create_announce', 
		kwargs={'news_id':page_list.program.id}))	# back to create.html

@permission_required('viewer.permission_admin')
@login_required
def add_pdf_plugin(request, page_id): # add pdf plugin to page that was selected.
	from cms.api import add_plugin, create_page
	from filer.models import File
	from django.contrib.auth.models import User
	page_list = get_object_or_404(PageList, pk=page_id)	# call PageList by id
	page = page_list.page	# call page by PageList above.
	
	if request.method == "POST": # pull data file from create.html
		MyPDFForm =PDFfileForm(request.POST, request.FILES)
		user = User.objects.get(username='root')
		if MyPDFForm.is_valid():

			if page_list.picture != None:	# if PageList have preview already mean that edit plugin then delete page and create new page
				
				page_list.picture.delete() # delete thumnail picture
				placeholders = list(page_list.page.get_placeholders())
				place = placeholders[0]
				plugins = place.get_plugins_list()
				if len(plugins) != 0: # if has plugin
					plug = plugins[0]
					instance, plugin = plug.get_plugin_instance()
					if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
						instance.delete()
					elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
						if instance.file != None:
							instance.file.delete()  # delete file plugin
					else: # if be image plugin or pdf plugin
						instance.file.delete()  # delete file plugin

				page.delete()
				web_page = create_page('web_page' + str(page_list.id), 'news.html', 'en') # create page
				page_list.page = web_page 
				page_list.prototype = None
				page_list.save()
				page = page_list.page

			pdf_file = File.objects.create(owner=user,
											original_filename='pdf_'+ page_list.name,
											file= MyPDFForm.cleaned_data["pdfUploader"])
			placeholder = page.placeholders.get(slot='content')
			add_plugin(placeholder, 'NewsPluginPDF', 'en', file=pdf_file) # set width, height and auto play
			page.publish('en')
			page.save()
		
			from django.core.files import File
			filename = 'Filetype-PDF-icon.png'	# file name
			filepath = 'media/media/page_preview/Filetype-PDF-icon.png'    # file path 
			with open(filepath, 'rb') as f:	# create empty page to be preview page 
				file_obj = File(f, name=filename)
				page_list.picture.save(str(page_list.name) + '.png', file_obj) # rename file following by PageList.name and set PageList.preview
				page_list.save()
	
	#get_preview_page(page_list, page) # get_preview_page()
	return HttpResponseRedirect(reverse('newsapp:to_create_announce', 
		kwargs={'news_id':page_list.program.id}))	# back to create.html

@permission_required('viewer.permission_admin')
@login_required
def add_video_plugin(request, page_id): # add video plugin to page that was selected.
	from cms.api import add_plugin, create_page
	from filer.models import File
	from django.contrib.auth.models import User
	page_list = get_object_or_404(PageList, pk=page_id)	# call PageList by id
	page = page_list.page	# call page by PageList above.
	
	if request.method == "POST": # pull data file from create.html
		MyVideoForm = VideofileForm(request.POST, request.FILES)
		MyURLVideoForm  = VideoURLForm(request.POST)
		user = User.objects.get(username='root')
		if MyVideoForm.is_valid(): # if using upload video

			if page_list.picture != None:	# if PageList have preview already mean that edit plugin then delete page and create new page
				
				page_list.picture.delete() # delete thumnail picture
				placeholders = list(page_list.page.get_placeholders())
				place = placeholders[0]
				plugins = place.get_plugins_list()
				if len(plugins) != 0: # if has plugin
					plug = plugins[0]
					instance, plugin = plug.get_plugin_instance()
					if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
						instance.delete()
					elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
						if instance.file != None:
							instance.file.delete()  # delete file plugin
					else: # if be image plugin or pdf plugin
						instance.file.delete()  # delete file plugin

				page.delete()
				web_page = create_page('web_page' + str(page_list.id), 'news.html', 'en') # create page
				page_list.page = web_page
				page_list.prototype = None
				page_list.save() 
				page = page_list.page

			video_file = File.objects.create(owner=user,
											original_filename='video_'+page_list.name,
											file= MyVideoForm.cleaned_data["VideoUploader"])
			placeholder = page.placeholders.get(slot='content')
			add_plugin(placeholder, 'VideoPluginPublisher', 'en', file=video_file, title='video_file_'+ page_list.name, width=1024, height=768) # set width, height and auto play
			page.publish('en')
			page.save()

			get_preview_page(page_list, page, request) # get_preview_page()

		elif MyURLVideoForm.is_valid() and request.POST["inputVideoURL"].strip(): # if using url video
			
			if page_list.picture != None:	# if PageList have preview already mean that edit plugin then delete page and create new page
				
				page_list.picture.delete() # delete thumnail picture
				placeholders = list(page_list.page.get_placeholders())
				place = placeholders[0]
				plugins = place.get_plugins_list()
				if len(plugins) != 0: # if has plugin
					plug = plugins[0]
					instance, plugin = plug.get_plugin_instance()
					if plug.plugin_type == 'TextPlugin': # if be TextPlugin then delete text
						instance.delete()
					elif plug.plugin_type == 'VideoPluginPublisher': # if be video plugin
						if instance.file != None:
							instance.file.delete()  # delete file plugin
					else: # if be image plugin or pdf plugin
						instance.file.delete()  # delete file plugin

				page.delete()
				web_page = create_page('web_page' + str(page_list.id), 'news.html', 'en') # create page
				page_list.page = web_page
				page_list.prototype = None
				page_list.save() 
				page = page_list.page
			
			url_geten = request.POST["inputVideoURL"]
			url_geten = url_geten.split('?')[0].split("/")[-1]
			video_url = "https://www.youtube.com/embed/"+url_geten+"?playlist="+url_geten+"&loop=3&autoplay=1&controls=0"
			
			placeholder = page.placeholders.get(slot='content')
			add_plugin(placeholder, 'VideoPluginPublisher', 'en', videoURL=video_url, title='video_url_'+ page_list.name, width=1024, height=768) # set width, height and auto play
			page.publish('en')
			page.save()
			
			from django.core.files import File
			filename = 'Filetype-youtube-icon.jpg'	# file name
			filepath = 'media/media/page_preview/Filetype-youtube-icon.jpg'    # file path 
			with open(filepath, 'rb') as f:	# create empty page to be preview page 
				file_obj = File(f, name=filename)
				page_list.picture.save(str(page_list.name) + '.jpg', file_obj) # rename file following by PageList.name and set PageList.preview
				page_list.save()

	return HttpResponseRedirect(reverse('newsapp:to_create_announce', 
		kwargs={'news_id':page_list.program.id}))	# back to create.html

def get_viewer(user, context):
	if UserViewer.objects.filter(user = user):
		viewer = UserViewer.objects.get(user = user)
		context['viewer'] =  viewer
	if user.has_perm('viewer.permission_admin'):
		context['is_admin'] = True
	elif user.has_perm('viewer.permission_viewer'): # viewer
		context['is_viewer'] = True
		context['allow_post'] = True
	else:
		context['is_viewer'] = True

def to_search(request):
	template_name = "FrontEnd/search.html"
	context = {}
	if request.user.is_active:
		user = request.user
		get_viewer(user, context)
	context['screens'] = get_list_screen
	return render(request, template_name, context)

def pagination_news(request): # paginate page when click next or previous button
	client_data = json.loads(request.GET.get('pagiante_data'))
	all_id = client_data['all_id'].split('a') # get id page list
	num_page = int(client_data['num_page']) # target num page
	sum_page = int(client_data['sum_page']) # sum of all news.
	news_geten = []
	for each_id in all_id:
		if PageList.objects.filter(id=int(each_id)): # if meet news by id
			news_geten.append(get_object_or_404(PageList, pk=int(each_id)))
	print(">>>>>>>> len news_geten: " + str(len(news_geten)))	
	paginator = Paginator(news_geten, 6)
	print("all_news_page: "+ str(paginator.num_pages))
	print("target_page: "+str(num_page))
	if paginator.num_pages < num_page: # if target page is more than range.
		page = paginator.page(paginator.num_pages)
		num_page = paginator.num_pages
	elif num_page == 0: # if target page is 0 (not be 0)
		page = paginator.page(1)
		num_page = 1
	else:
		page = paginator.page(num_page) # get news from target page

	news_list = []
	for p in page:
		news_data = [p.url, p.picture.url]
		news_list.append(news_data)

	can_next = "has_next" if page.has_next() else "not_next"
	can_previous = "has_previous" if page.has_previous() else "not_previous"

	list_id = ""
	for each_news in news_geten: # get all page id 
		list_id = list_id + str(each_news.id) + "a"
	list_id = list_id[:-1] # cut last : out

	server_data = {'all_id':list_id, 'has_next':can_next, 'has_previous':can_previous, 
					'current_page':num_page, 'sum_page':sum_page, 'news_list': news_list,
					 'num_all_page':paginator.num_pages} # send delete_index back for delete page in create.html
	return JsonResponse(server_data) 

def paginate_news(all_news, current=1): # use for show news only first 6 news
	context = {}
	paginator = Paginator(all_news, 6)
	page = paginator.page(current) # get first 6 news

	context['page_num'] = paginator.page_range
	context['show_news'] = page.object_list
	context['page_now'] = current
	context['num_page_news'] = page
	return context

def search_news(request): # search news by tags or name or datetime
	from datetime import datetime, date, time
	template_name = "FrontEnd/search.html"
	context = {}

	if request.user.is_active:
		user = request.user
		get_viewer(user, context)

	context['screens'] = get_list_screen
	tags = ["tag1", "tag2", "tag3", "tag4"]
	news_geten = PageList.objects.all()
	print("++++++++++++++++++++++++++++")
	if request.method == "GET":
		MySearchForm = SearchNewsForm(request.GET)
		print(MySearchForm.is_valid())
		if MySearchForm.is_valid():
			print(MySearchForm.cleaned_data)

			program = NewsPage.objects.all()

			# part search by time.
			begin_date = MySearchForm.cleaned_data["begin_date"]
			begin_time = MySearchForm.cleaned_data["begin_time"]
			end_date = MySearchForm.cleaned_data["end_date"]
			end_time = MySearchForm.cleaned_data["end_time"]

			if ( begin_date != '' and begin_time != '' ) or ( end_date != '' and end_time != '' ):
				print("begin_date :" + MySearchForm.cleaned_data["begin_date"])
				print("begin_time :" + MySearchForm.cleaned_data["begin_time"])
				print("end_date :" + MySearchForm.cleaned_data["end_date"])
				print("end_time :" + MySearchForm.cleaned_data["end_time"])
				if ( begin_date != '' and begin_time != '' ) and ( end_date == '' and end_time == '' ): # if fill only begin datetime.
					b_date = begin_date.split('-') # read date value
					b_time = begin_time.split(':') # read time vale
					begin_d = date(int(b_date[2]), int(b_date[1]), int(b_date[0])) # create date 
					begin_t = time(int(b_time[0]), int(b_time[1]) ) # create time
					begin_datetime = datetime.combine(begin_d, begin_t) # create datetime
					program = program.filter(start_time__gte = begin_datetime ) 
				elif ( begin_date == '' and begin_time == '' ) and ( end_date != '' and end_time != '' ): # if fill only end datetime
					e_date = end_date.split('-') # read date value
					e_time = end_time.split(':') # read time vale
					end_d = date(int(e_date[2]), int(e_date[1]), int(e_date[0]) )  # create date
					end_t = time(int(e_time[0]), int(e_time[1]) ) # create time
					end_datetime = datetime.combine(end_d, end_t) # create datetime
					program = program.filter(start_time__lte = end_datetime )
				elif ( begin_date != '' and begin_time != '' ) and ( end_date != '' and end_time != '' ): # if fill both begin datetime and end datetime.
					b_date = begin_date.split('-') # read date value
					b_time = begin_time.split(':') # read time vale
					begin_d = date(int(b_date[2]), int(b_date[1]), int(b_date[0]) ) # create date
					begin_t = time(int(b_time[0]), int(b_time[1]) ) # create time
					begin_datetime = datetime.combine(begin_d, begin_t) # create datetime

					e_date = end_date.split('-') # read date value
					e_time = end_time.split(':') # read time vale
					end_d = date(int(e_date[2]), int(e_date[1]), int(e_date[0]) ) # create date
					end_t = time(int(e_time[0]), int(e_time[1]) ) # create time
					end_datetime = datetime.combine(end_d, end_t) # create datetime

					program = program.filter(start_time__lte = begin_datetime ).filter(end_time__gte = end_datetime)
					#program = program.filter(start_time__range = ( begin_datetime, end_datetime) )
				news_geten = news_geten.filter(program=program)
			# part search by screen.	
			search_screen = MySearchForm.cleaned_data["search_screen"]
			if search_screen:
				print("search_screen :" + MySearchForm.cleaned_data["search_screen"])
				screen = Screen.objects.get(name=search_screen)
				program = NewsPage.objects.filter(screen=screen)
				news_geten = news_geten.filter(program=program)

			# part search by name.
			search_name = MySearchForm.cleaned_data["search_name"]
			if search_name != '':
				program = program.filter(name=search_name) # get program list by search name.
				if len(program): # if found by search name.
					print("search_name :" + MySearchForm.cleaned_data["search_name"])
					news_geten = news_geten.filter(program=program)

			# part search by tag
			tag1 = MySearchForm.cleaned_data["tag1"]
			tag2 = MySearchForm.cleaned_data["tag2"]
			tag3 = MySearchForm.cleaned_data["tag3"]
			tag4 = MySearchForm.cleaned_data["tag4"]
			news_tag = [] # collect news by tag
			meet_tag = False
			if tag1:
				print(MySearchForm.cleaned_data["tag1"])
				meet_tag = True
				pages = news_geten.filter(tag = u'ภาควิชา')
				for page in pages:
					news_tag.append(page)
			if tag2:
				print(MySearchForm.cleaned_data["tag2"])
				meet_tag = True
				pages = news_geten.filter(tag = u'กิจกรรม')
				for page in pages:
					news_tag.append(page)
			if tag3:
				print(MySearchForm.cleaned_data["tag3"])
				meet_tag = True
				pages = news_geten.filter(tag = u'ทั่วไป')
				for page in pages:
					news_tag.append(page)
			if tag4:
				print(MySearchForm.cleaned_data["tag4"])
				meet_tag = True
				pages = news_geten.filter(tag = u'รับสมัครงาน/ฝึกงาน')
				for page in pages:
					news_tag.append(page)
			if meet_tag:
				news_geten = news_tag

	new_news_geten = []
	for each_news in news_geten: # loop check no plugin page
		placeholders = list(each_news.page.get_placeholders())
		if placeholders[0].get_plugins_list() and (each_news.prototype == None): # if have plugin
			new_news_geten.append(each_news) # add to new list	
	news_geten = new_news_geten 

	list_id = ""
	for each_news in news_geten: # get all page id 
		list_id = list_id + str(each_news.id) + "a"
	context["all_page_id"] = list_id[:-1] # cut last : out

	context.update(paginate_news(news_geten))
	context["pagination_check"] = 1
	context["pagination_counter"] = len(news_geten)
	return render(request, template_name, context)

#@permission_required('viewer.permission_admin')
@login_required
def copy_program(request, page_id): # duplicate program
	from cms.api import create_page
	from django.core.files import File
	news_geten = get_object_or_404(NewsPage, pk=int(page_id))
	context = {}
	template_name = 'newsapp/table.html'
	print('>>>>>>>>>>>>>>>>>>>>>>>>> copy_program')
	list_news= []
	screen_obj = news_geten.screen
	list_program = NewsPage.objects.filter(screen=screen_obj) # get news from screen
	for each_program in list_program: 
		list_news.append(each_program)
	context['table_news'] = list_news # for show news in table
	if request.method == 'POST':
		if request.POST["copy_name"]:
			if NewsPage.objects.filter(name=request.POST["copy_name"]): # if this name repeated with other.
				context['error_message'] = 'this name is used already.'
				context['id_error'] = page_id
				return render(request, template_name, context)
			else: # duplicate
				news_copy = NewsPage() # new program
				if request.POST["copy_name"] != '':  # create name
					news_copy.name = request.POST["copy_name"]
				if news_geten.start_time != '': # copy start_time +7 hours or not
					news_copy.start_time = news_geten.start_time
				if news_geten.end_time != '': # copt end_time +7 hours or not
					news_copy.end_time = news_geten.end_time
				if news_geten.screen != '': # copy screen
					news_copy.screen = news_geten.screen
				news_copy.save()
				web_pages = PageList.objects.filter(program=news_geten) # get page for copy
				for web_page in web_pages: # loop for copy each page
					copy_web = PageList()
					copy_web.index = web_page.index
					copy_web.program = news_copy
					copy_web.prototype =  web_page.id
					copy_web.save() # save copy PageList

					if web_page.interval != '': # copy interval
						copy_web.interval = web_page.interval
					if web_page.tag != '':
						copy_web.tag = web_page.tag
					page_copy = create_page('web_page' + str(copy_web.id), 'news.html', 'en') # create page
					copy_web.page = page_copy
					page_copy.publish('en')
					copy_web.url = page_copy.get_public_url('en') + '?edit_off'
					copy_web.name = 'page'+str(copy_web.id) # set name of new_page
					copy_web.save() # save copy PageList

					filename = web_page.picture.name
					filepath = web_page.picture.path
					with open(filepath, 'rb') as f: # save picture
						file_obj = File(f, name=filename)
						copy_web.picture.save(str(copy_web.name)+ '.png', file_obj)
						copy_web.save()

					placeholders = list(web_page.page.get_placeholders()) # get placeholder from web_page
					place = placeholders[0] # select only first placeholder because one page has one placeholder
					plugins = place.get_plugins_list() # get plugin from placeholder
					if plugins: # if web_page has plugin
						web_page.page._copy_contents(page_copy, 'en') # copy plugin to copy_web	
						page_copy.publish('en')
						page_copy.save()
					copy_web.save()
				print('>>>>>>>>>>>>>>>>>>>>>>>>> duplicate')
	return HttpResponseRedirect(reverse('newsapp:get_time_table', 
		kwargs={'screen_id':screen_obj.id}))	# back to table.html

@permission_required('viewer.permission_admin')
@login_required
def edit_text(request): # get old content before edit Text plugin content.
	client_data = json.loads(request.GET.get('edit_data'))
	client_data = json.loads(client_data)
	print(client_data)
	
	page_id = int(client_data["edit_id"]) 
	page_edit = get_object_or_404(PageList, pk=page_id) # get page follow by id
	page_edit = page_edit.page
	placeholders = list(page_edit.get_placeholders()) # get placeholder
	plugins = placeholders[0].get_plugins_list()[0] # get plugin
	instance, plugin = plugins.get_plugin_instance() # get text plugin
	server_data = {'old_text': instance.body} # get content 
	return JsonResponse(server_data)  # send back a content.

def check_online(request): # client check server online
	client_data = json.loads(request.GET.get('msg'))
	return JsonResponse({'msg':"success"})

def check_symbol_page(request):
	from datetime import datetime, date, time, timedelta
	utc = pytz.utc # get tzinfo UTC
	client_data = json.loads(request.GET.get('symbol'))
	screen_geten = Screen.objects.get(name = client_data['screen'])
	news_geten = NewsPage.objects.filter(screen = screen_geten, start_time__isnull = False, end_time__isnull = False)#, publish = True)
	
	meet_news =  False
	if len(news_geten) > 0:
		for pub_news in news_geten: # loop check every programs
			if pub_news.was_published() and pub_news.is_completed(): # if have publish program
				# redirect to announce_screen
				return HttpResponseRedirect(reverse('newsapp:screen_announce', 
					kwargs={'screen_text':client_data['screen'], 'time_text':client_data['target_url']}))
		if not meet_news: # check next 15 seconds if has publish program in the future
			next_time = datetime.now().replace(tzinfo=utc) - timedelta(hours=7)+ timedelta(seconds=15)
			news_geten = news_geten.filter(start_time__gte = next_time).order_by('start_time')
			refresh_time = 15
			symbol_page = "symbol_page"
			if len(news_geten) > 0: # adjust refresh time to match with next publish program
				refresh_time = (news_geten[-1].start_time - next_time).seconds # get the first news in time.
				symbol_page = "None"
			return JsonResponse({'refresh_time':refresh_time, 'symbol_page':symbol_page})
	else:
		next_time = datetime.now().replace(tzinfo=utc) - timedelta(hours=7)+ timedelta(seconds=15)
		news_geten = news_geten.filter(start_time__gte = next_time).order_by('start_time')
		refresh_time = 15
		symbol_page = "symbol_page"
		if len(news_geten) > 0: # adjust refresh time to match with next publish program
			refresh_time = (news_geten[-1].start_time - next_time).seconds # get the first news in time.
			symbol_page = "None"
		return JsonResponse({'refresh_time':refresh_time, 'symbol_page':symbol_page})