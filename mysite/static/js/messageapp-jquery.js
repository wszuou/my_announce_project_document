$(document).ready(function(){
	/*/
	show message 
	1. check message form must not empty(warn by alert)when click post.
	2. get message'age time from server 
	3. get right message from server
	4. wait for message'age time is timeout.
	5. loop 2-5
	/*/
	var message_delay =15000;
	setInterval(get_present_message, message_delay);
	// update new message when message timeout. 
	console.log("first message_delay: "+message_delay);
	get_present_message();
	startTime();
	var sec = 0;
	var count = 0;

	function get_present_message(){	
		var request_data={'key':'value'};
		$.ajax({
			url:'../../../../messageapp/message.json',
			type: 'GET',
			data:{ 
				client_data: JSON.stringify(request_data)
			},
			success: function(server_message){
				var result = JSON.stringify(server_message);
				var message_obj = JSON.parse(result);
				$.each(message_obj.server_data, function(i, field){
					//$("p.server_data").append(i +":"+ field.message + ", ");
					if( i == 0 ){ // get message that was published.
						console.log(field.message)
						$("div.message_posted").empty();
						$("div.message_posted").append(field.message);
						count++;
					}
					else if(i == 1){ // get start time of message
						//$("div.message_time").empty();
						//$("div.message_time").append(field.message);
					}
					else{ // get delay time to update new message.
						message_delay = parseInt(field.message);
						$("p.message_delay").empty();
						$("p.message_delay").append(field.message);
					}
				});
			},

	        error: function(){ // if offline
	        	console.log('error send message');
	        }
		});
	};

	function startTime() {
	    var today =  new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    m = checkTime(m);
	    s = checkTime(s);
		$("div.message_time").empty();
		$("div.message_time").append(h + ":" + m + ":" + s);
	    //console.log(h + ":" + m + ":" + s);
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}

});