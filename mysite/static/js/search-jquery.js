$(document).ready(function(){
	$('a[name=previous_page]').hide();
	$('a[name=previous_page]').click(function(){
		var now_page = $('div.pagination_check').attr('id');
		console.log('now_page before send: '+ now_page);
		to_anouther_page(parseInt(now_page)-1);
	});
	$('a[name=next_page]').click(function(){
		var now_page = $('div.pagination_check').attr('id');
		console.log('now_page before send: '+ now_page);
		to_anouther_page(parseInt(now_page)+1);
	});
	$('input[name=begin_date]').change(function(){ // if fill begin_date then end_date will fill the same data. 
		console.log("begin date change: "+ $('input[name=begin_date]').val().toString() );
		$('input[name=end_date]').val($('input[name=begin_date]').val())
	});
	$('input[name=begin_time]').change(function(){ // if fill begin_time then end_time will plus 1 hour from begin_time.
		console.log("begin time change: "+ $('input[name=begin_dtime]').val().toString() );
		var begin_time = $('input[name=begin_time]').val().toString();
		next_time = ""; // next_time = plus 1 hour from begin_time.
		$('input[name=end_time]').val(next_time);
	});
})

function to_anouther_page(num_page){
	/* get num_page to pagination.By use ajax to send num_page as
	a target page and all_page_id.When send data is success then 
	change content to show.
	*/
	var all_id = $('div.all_page_id').attr("id");
	var sum_page = $('div.pagination_counter').attr("id");
	var request_data = {'all_id':all_id, 'num_page':num_page, 'sum_page':sum_page};
	
	console.log("click to page: " + num_page.toString());

	$.ajax({
		url:'../pagination_news.json',
		type: 'GET',
		data:{
			pagiante_data: JSON.stringify(request_data)
		},
		success: function(server_response){
			var result = JSON.stringify(server_response);
            var message_obj = JSON.parse(result);
            $('div[id=announce]').empty(); //remove old news that was showed.
            //$('div[name=paginate_tag]').empty(); // remove old pagination.
			$('a[name=previous_page]').hide(); // hide previous page button
			$('a[name=next_page]').hide(); // hide next page button
			$('div.pagination_counter').attr('id', message_obj.sum_page);
			$('div.all_page_id').attr('id', message_obj.all_id);
			$('div.pagination_check').attr('id', message_obj.current_page);
			console.log('>>>>>>>>>>>><<<<<<<<<<<<');
			console.log($('div.pagination_counter').attr('id'));
			console.log($('div.all_page_id').attr('id'));
			console.log($('div.pagination_check').attr('id'));
			show_new_tag = ''
			message_obj.news_list.forEach(function(each_news, index){
				if((index == 0)||(index == 3)){ // start new line
					show_new_tag+='<div class="w3-row-padding">';
					show_new_tag+='<div class="w3-third w3-container w3-margin-bottom w3-border w3-hover-border-red">'+
									'<a href="'+each_news[0]+'" target="_blank" name="link_page">'+
            '<img src="'+ each_news[1] +'" style="width:100%;height: 16em" class="w3-hover-opacity"></a>'+
        	'<div class="w3-container w3-white"><p><b>'+(index+1).toString()+'</b></p></div></div>';
				}
				else if(index==2){ // end line
					show_new_tag+='<div class="w3-third w3-container w3-margin-bottom w3-border w3-hover-border-red">'+
									'<a href="'+each_news[0]+'" target="_blank" name="link_page">'+
            '<img src="'+ each_news[1] +'" style="width:100%;height: 16em" class="w3-hover-opacity"></a>'+
        	'<div class="w3-container w3-white"><p><b>'+(index+1).toString()+'</b></p></div></div>  </div>';
				}
				else{
					show_new_tag+='<div class="w3-third w3-container w3-margin-bottom w3-border w3-hover-border-red">'+
									'<a href="'+each_news[0]+'" target="_blank" name="link_page">'+
            '<img src="'+ each_news[1] +'" style="width:100%;height: 16em" class="w3-hover-opacity"></a>'+
        	'<div class="w3-container w3-white"><p><b>'+(index+1).toString()+'</b></p></div></div>';
				}
			});
			show_new_tag+='</div>'
			$('div[id=announce]').append(show_new_tag);

			pagination_tag = '<ul id="pagination" class="w3-pagination">'
			if (message_obj.has_previous=="has_previous"){
				$('a[name=previous_page]').show();
				console.log(message_obj.has_previous);
			}
			$('a[name=num_of_all]').text('Page '+message_obj.current_page+' of '+message_obj.num_all_page);
			if (message_obj.has_next=="has_next"){
				$('a[name=next_page]').show();
				console.log(message_obj.has_next);
			}
			pagination_tag+='</ul>'
			//$('div[name=paginate_tag]').append(pagination_tag);
			console.log(message_obj.current_page);
			console.log($('div.pagination_check').attr('id'));
			console.log("send data ajax to server...");
		}
	});
	
}