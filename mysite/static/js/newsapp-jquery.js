$(document).ready(function(){
	$.getScript("../../../static/datetimepicker/src/DateTimePicker.js", function() {
    	$("#datetimeInput").DateTimePicker();
    });
    $.getScript("../../../../static/datetimepicker/src/DateTimePicker.js", function() {
        $("#datetimeInput").DateTimePicker();
    });

    $("#TextBtn").click(function(){ // open text plugin modal
        $.getScript("../../../../static/ckeditor/ckeditor.js", function(){
            CKEDITOR.instances['editor1'].setData(''); // set to be blank text area.
        });
        $("#TextModal").modal();
    });

    $("div[name=spiner]").hide(); // hide process spiner.
    $('button[name=text_submit]').click(function(){
        $("#SpinerModal").modal(); // show spiner while click
        $('div[name=spiner]').show();
    });
    $('button[name=image_submit]').click(function(){
        $("#SpinerModal").modal(); // show spiner while click
        $('div[name=spiner]').show();
    });
    $('button[name=pdf_submit]').click(function(){
        $("#SpinerModal").modal(); // show spiner while click
        $('div[name=spiner]').show();
    });
    $('button[name=video_submit]').click(function(){
        $("#SpinerModal").modal(); // show spiner while click
        $('div[name=spiner]').show();
    });

    // check file type in ImageModal
    $("input[name=ImageUploader]").change(function(){
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        var f = this.files[0];
        console.log($(this).val());
        // if invalid file type or file size is bigger than 20MB
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1 || f.size > 2097152){ 
            console.log('file: '+ f.size.toString());
            if (f.size > 2097152){ // 20480 test 20kB
                console.log("file size is bigger than 2MB");
                $('div[name=alert-ImageUploader]').html("<div class='alert alert-danger'>"+'<strong>File size error!</strong>'+
                ' this file size is bigger than 2MB'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file size not support.
            }
            else{
                console.log("wrong file type.");
                $('div[name=alert-ImageUploader]').html("<div class='alert alert-danger'>"+'<strong>File type error!</strong>'+
                ' please use file type: .jpeg, .jpg, .png, .gif, .bmp'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file type not support. 
            }
            $("input[name=ImageUploader]").val(""); // set input field to empty.
        }
    });
    // check file type in pdfModal
    $("input[name=pdfUploader]").change(function(){
        var fileExtension = ['pdf'];
        var f = this.files[0];
        console.log($(this).val());
        // if invalid file type or file size is bigger than 20MB
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1 || f.size > 20971520){ 
            console.log('file: '+ f.size.toString());
            if (f.size > 20971520){ // 20480 test 20MB
                console.log("file size is bigger than 20MB");
                $('div[name=alert-pdfUploader]').html("<div class='alert alert-danger'>"+'<strong>File size error!</strong>'+
                ' this file size is bigger than 20MB'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file size not support.
            }
            else{
                console.log("wrong file type.");
                $('div[name=alert-pdfUploader]').html("<div class='alert alert-danger'>"+'<strong>File type error!</strong>'+
                ' please use file type: .pdf'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file type not support. 
            }
            $("input[name=pdfUploader]").val(""); // set input field to empty.
        }
    });
    // check file type in VideoModal
    $("input[name=VideoUploader]").change(function(){
        var fileExtension = ['mp4', 'ogg', 'webm'];
        var f = this.files[0];
        console.log($(this).val());
        $("input[name=inputVideoURL]").val(""); // set video field to empty because using video uploader
        // if invalid file type or file size is bigger than 200MB
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1 || f.size > 2147483648){ 
            console.log('file: '+ f.size.toString());
            if (f.size >  2147483648){ // 20480 test 20kB
                console.log("file size is bigger than 2GB");
                $('div[name=alert-VideoUploader]').html("<div class='alert alert-danger'>"+'<strong>File size error!</strong>'+
                ' this file size is bigger than 2GB'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file size not support.
            }
            else{
                console.log("wrong file type.");
                $('div[name=alert-VideoUploader]').html("<div class='alert alert-danger'>"+'<strong>File type error!</strong>'+
                ' please use file type: .mp4 or .ogg'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file type not support. 
            }
            $("input[name=VideoUploader]").val(""); // set input field to empty.
        }
    });
    
    $("input[name=inputVideoURL]").change(function(){ // if using video url.
        $("input[name=VideoUploader]").val(""); // set video uploader to empty because using video url.
    });

    $("input[name=time_field]").change(function(){ // read data in form while typing.
        var stt = $(this).val();
        var selected_time_field = $("li.highlight").attr("id").toString();
        selected_time_field = selected_time_field.split('&');
        $("li.highlight").attr("id", selected_time_field[0]+'&'+ stt.toString()+'&' +selected_time_field[2]);
        console.log($("li.highlight").attr("id"));
    }); 
     /*   
    function tags_change(sel){ // read data form tag field.
        var stag =  sel.value; //$(this).val();
        var selected_tags_field = $("li.highlight").attr("id").toString();
        selected_tags_field = selected_tags_field.split('&');
        $("li.highlight").attr("id", selected_tags_field[0] + '&' + selected_tags_field[1] + '&' + stag.toString());
        console.log("change tags >>>>>>>>>>>>>>>>");
        console.log($("li.highlight").attr("id"));
    } */

    meetSelected();
    $.getScript("../../../../static/html5sortable/jquery.sortable.js", function(){

        $("button[name=plus_button]").click(function(){ //create new page.
          console.log("clcik-add-new-page.");
          var program_id = $('input[name=name_field]').attr("id"); // get id program from input name.
          console.log(program_id);
          var request_data = {'program_id':program_id.toString()}; // data that send to server.
          $.ajax({
            url:'../../add_new_page.json',
            type: 'GET',
            data:{
                newspage_data: JSON.stringify(request_data)
            },
            success: function(server_response){
              var result = JSON.stringify(server_response);
              var message_obj = JSON.parse(result);
              var childs =$("#page-viewer").find("li").length; // get number of li in ul.
              console.log("success");
              console.log(message_obj.page_id);
              // create new page
              $("#page-viewer").append('<li onclick="toggleSelected(this)" id="' + message_obj.page_id + 
                '&' + message_obj.page_interval + '&' + message_obj.page_tag +'"> <p>'+(childs+1).toString()+'</p>'+
                '<a href="'+ message_obj.page_url +'" target="frame_example" name="link_page" >'+
                '<img src="'+ message_obj.picture_url +'" width="180px" height="120px"></a></li>');
              $(function() {
                $('.sortable').sortable();
                $('.handles').sortable({
                  handle: 'span'
                });
                $('.connected').sortable({
                  connectWith: '.connected'
                });
                $('.exclude').sortable({
                  items: ':not(.disabled)'
                });
              });
            }
          });
        });

        $("button[name=delete_button]").click(function(){ // click delete modal.
            var program_id = $("input[name=name_field]").attr("id").toString(); // get program id
            var page_id = $("button[name=delete_button]").attr("id").split('&')[0]; // page id that be deleted.
            var request_data = {'program_id':program_id, 'page_id':page_id};
            console.log(request_data);
            $.ajax({
                url:'../../delete_page_select.json',
                type:'GET',
                data:{
                    newspage_delete: JSON.stringify(request_data)
                },
                success: function(server_response){
                    var result = JSON.stringify(server_response);
                    var data_obj = JSON.parse(result);
                    console.log("delete success");
                    
                    $("#page-viewer").find("li").each(function(i){ // remove li that was select
                        if ($('p',this).text() == data_obj.delete_index){
                            $('p',this).remove();
                        }
                    });   
                    
                    $("#page-viewer").find("li").each(function(i){ // adjust number of page.
                        $('p',this).text((i+1).toString());// select p inside this.
                    });
                    
                    $(function() { // enable sortable.
                      $('.sortable').sortable();
                      $('.handles').sortable({
                        handle: 'span'
                      });
                      $('.connected').sortable({
                        connectWith: '.connected'
                      });
                      $('.exclude').sortable({
                        items: ':not(.disabled)'
                      });
                    });
                    location.reload();
                }
            });

        });
    });

    $("button[name=save_button]").click(function(){ // click save button
        console.log("click saved");
        saveAnnounce("save_button");
    }); 
    $("button[name=update_button]").click(function(){ // click update button
        console.log("click update");
        saveAnnounce("update_button");
    })
    $("button[name=publish_button]").click(function(){ // click publish button
        console.log("click publish");
        saveAnnounce("publish_button");
    })
    $("button[name=backToDraft_button]").click(function(){ // click back to draft button
        console.log("backToDraft_button");
        saveAnnounce("backToDraft_button");
        // send id program to server to set publish to False.
        program_id = $("input[name=name_field]").attr("id").toString()
        request_data = {'program_id': program_id};
        $.ajax({
          url:'../../to_create_announce/page_to_draft.json',
          type: 'GET',
          data:{
              newspage_data: JSON.stringify(request_data)
          },
          success: function(server_response){
            location.reload(); // refresh page
          }
        });

    })

    var example = $("div.example-page-mark");
    if (example.length > 0){
    }
    //   working in show.html to control refresh time then go to 
    // views.screen_announce for get new annonuce.

    /*/ working when render create.html(create NewsPage object) use for check 
      error in field form.
    	1. check white space.
    	2. check error of start_time and end_time field by send request ajax
    	    to sever and get error warning.
    	3. if fill form start_time then must also fill form end_time
    	4. if fill form end_time then must also fill form start_time
    	5. NewsPage's name should be unique checked by ajax to server
    	all of this will action when clicked submit button. 
    /*/

    /*/ create screen.html(create Screen object) check error in field form.
		1. check white space.
		2. Screen's name should be unique
    /*/ 
});

function meetSelected(){
    var meet_selected = $("li.highlight");
    if (meet_selected.length > 0){ // work when click new program.
        var selected_id = $("li.highlight").attr("id").toString();
        var selected_page = selected_id.split("&");
        selected_id = selected_id.split("&")[0]
        $("form[name=TextPlugin]").attr("action", "../../"+ selected_id + "/add_text_plugin/");// add action url with page id to Text plugin.
        $("form[name=ImagePlugin]").attr("action", "../../"+ selected_id +"/add_image_plugin/");// add action url with page id to Image plugin.
        $("form[name=pdfPlugin]").attr("action", "../../" + selected_id + "/add_pdf_plugin/");// add action url with page id to PDF plugin.
        $("form[name=VideoPlugin]").attr("action", "../../" + selected_id +"/add_video_plugin/");// add action url with page id to Video plugin.
        $("button[name=delete_button]").attr("id", selected_id);// add page id to delete modal.
        
        $("a[name=preview_button]").attr("href", $("a[name=link_page]").attr("href"));
        console.log("find highlight: " + $("form[name=ImagePlugin]").attr("action").toString() );
        if (selected_page[1] != "0:00:00"){
            $("input[name=time_field]").val(selected_page[1]);
        }
        if (selected_page[2] != "None"){
            $("select[name=tags_field]").val(selected_page[2]);
        }
    
    }
}

function toggleSelected(el){
    $("#page-viewer").find("li").each(function(){
        var product = $(this);
        product.removeClass('highlight');
        console.log(product.text());
        // get id of page that selected then put it to all plugin and garbage button.
    });
    el.className = "highlight";
    var selected_id = el.id.split('&')[0];
    
    $("form[name=TextPlugin]").attr("action", "../../"+ selected_id + "/add_text_plugin/");// add action url with page id to Text plugin.
    $("form[name=ImagePlugin]").attr("action", "../../"+ selected_id +"/add_image_plugin/");// add action url with page id to Image plugin.
    $("form[name=pdfPlugin]").attr("action", "../../"+ selected_id +"/add_pdf_plugin/");// add action url with page id to PDF plugin.
    $("form[name=VideoPlugin]").attr("action", "../../" + selected_id +"/add_video_plugin/");// add action url with page id to Video plugin.
    $("button[name=delete_button]").attr("id", selected_id);// add page id to delete modal.
    
    $("a[name=preview_button]").attr("href", $("li.highlight > a").attr("href"));
    console.log("toggleSelected " + el.id.toString());
    if (el.id.split('&')[1] != "0:00:00"){
        $("input[name=time_field]").val(el.id.split('&')[1]);
    }
    if (el.id.split('&')[2] != "None"){
        $("select[name=tags_field]").val(el.id.split('&')[2]);
        console.log("toggle tags ========= not None");
    } else{
        $("select[name=tags_field]").val('');
        console.log('toggle select tag >>>>>>>>>>>>');
        console.log($("select[name=tags_field]").val());
    }
  
    $('iframe[name=frame_example]').load(function(){ // add some text to iframe for make symbol that stay in create page.
        console.log("iframe loaded");
        $('iframe[name=frame_example]').contents().find("body").append('<div class="newsapp_create_page"></div>');
    });
};

function saveAnnounce(el){ // read data form and convert to json.
    if (( $.trim($('input[name=name_field]').val() ).length > 0 ) && 
        ( $.trim($('input[name=start_date_field]').val() ).length > 0 ) && ( $.trim($('input[name=start_time_field]').val() ).length > 0 ) &&
        ( $.trim($('input[name=end_time_field]').val() ).length > 0 ) ){ // check whitespace.
        console.log("is not whitespace.");
   
        var editInfo = '{"Program" : ['+
            '{"name":"' +$('input[name=name_field]').val() + '",' + 
            '"id":"'+$('input[name=name_field]').attr("id") +'",'+
            '"screen":"'+$('select[name=screen_field]').val() +'",'+
            '"start_datetime":"'+$('input[name=start_date_field]').val()+'&' +$('input[name=start_time_field]').val()+'",'+
            '"end_datetime":"'+$('input[name=start_date_field]').val()+'&'+$('input[name=end_time_field]').val() +'"'+
            '}], "button":"'+ el +'","PageList":['; // add id program and page list. el is button name that clcik.
        $("#page-viewer").find("li").each(function(i){ // collect data for each page.
            var product = $(this).attr("id");
            var data_page = product.split("&");
            if (i == $('#page-viewer').find("li").length - 1){ // if last index no have , in then end
                editInfo += '{"id":"'+ data_page[0] +'",' +
                            '"interval":"'+ data_page[1] +'",' +
                            '"index":"'+ i.toString() +'",' +
                            '"tag":"'+ data_page[2] +
                            '"}]}';
            } else{
                editInfo += '{"id":"'+ data_page[0] +'",' +
                            '"interval":"'+ data_page[1] +'",' +
                            '"index":"'+ i.toString() +'",' +
                            '"tag":"'+ data_page[2] +
                            '"},';
            }
        });
        console.log(editInfo);
        var obj = JSON.parse(editInfo);
        console.log(obj);
        $.ajax({
            url:'../../to_create_announce/edit_program.json',
            type: 'GET',
            data:{
                news_data:JSON.stringify(editInfo)
            },
            success: function(server_response){
                var result = JSON.stringify(server_response);
                var message_obj = JSON.parse(result);
                console.log(message_obj.button);
                if (message_obj.message == "success"){ // if success then check button that click.
                    if ((message_obj.button == "save_button")||(message_obj.button == "backToDraft_button")){
                        // still be in create.html then alert success  
                        console.log("save success");
                        /*/
                        $('div[name=alert-something]').html("<div class='alert alert-success'>"+'<strong>Success!</strong>'+"  Data have saved already."+
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");
                        /*/
                    }
                    else { // if update button or publish button
                        // go to programs.html
                        window.location.href = "/newsapp/";
                    }

                } else{ // if have error then still be in create.html and tell user a error message.
                    $('div[name=alert-something]').html("<div class='alert alert-danger'>"+'<strong>Failure!</strong>'+message_obj.error_text+
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>"); 
                }

            }
        });
        $("#page-viewer").find("li").each(function(i){
            console.log(i.toString()+": " + $(this).text());
        });
    
    }else{ // is whitespace
        console.log("whitespace.");
        $('div[name=alert-something]').html("<div class='alert alert-warning'>"+'<strong>Warning!</strong>'+"please insert completely program's name, start time and end time."+
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");
    }
};

function EditTextData(el){ // get old data of text plugin
    console.log("Edit page data: "+ el.toString());
    editInfo = '{"edit_id":"' + el.toString() + '"}';
    $.ajax({
        url:'../../to_create_announce/edit_text.json',
        type: 'GET',
        data:{
            edit_data:JSON.stringify(editInfo)
        },
        success: function(server_response){
            var result = JSON.stringify(server_response);
            var message_obj = JSON.parse(result);
            console.log(message_obj.old_text);
            $.getScript("../../../../static/ckeditor/ckeditor.js", function(){
                CKEDITOR.instances['editor1'].setData(message_obj.old_text);
            });
            $("#TextModal").modal();
            //$('textarea[name=editor1]').val(message_obj.old_text)
        }
    });
    
};

