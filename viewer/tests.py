#-*- coding: utf-8 -*
import unittest
from django.core.urlresolvers import reverse
from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from .models import UserViewer
from viewer.models import UserViewer

from django.utils import timezone
from django.utils.timezone import localtime
import datetime
from django.test.utils import setup_test_environment
from django.contrib.auth import authenticate, login, logout
from filer.models import Image, File
from django.contrib.auth.models import Permission, User
from django.core.files.uploadedfile import SimpleUploadedFile

setup_test_environment()

class Test_add_user_scv(unittest.TestCase):
	def setUp(self):
		self.client = Client()

	def test_upload_user(self):
		from django.core.files import File
		self.assertEqual(len(UserViewer.objects.all()), 0) 
		with open("file_test/username.csv", "rb") as csvfile:
			csv_file = SimpleUploadedFile('username.csv', csvfile.read())
			response = self.client.post(reverse('viewer:add_user_csv'), {'userUploader': csvfile.read()})
			self.assertEqual(response.status_code, 302)
		self.assertEqual(len(UserViewer.objects.all()), 0) # user csv was added.

	def test_valid_data_user(self):
		users = UserViewer.objects.all()
		for user in users:
			user_data = str(user.user.username)+', '+str(user.user.password)+', '+str(user.user.first_name)+', '+str(user.user.last_name)
			print(user_data)
			self.assertEqual(user.has_perm('viewer.permission_viewer'), True)

class Test_disallow_user(unittest.TestCase):
	def setUp(self):
		self.client = Client()
		cli_user = User.objects.create_user(username='5501012634444',
											first_name='Napat',
											last_name='RodTang',
											password="1234")	
		permission = Permission.objects.get(codename='permission_viewer') # add permission viewer
		cli_user.user_permissions.add(permission)
		self.viewer = UserViewer(user = cli_user) # create example user

	def test_permission_before_disallow(self):
		user_test = User.objects.get(username='5501012634444')
		cli_user = UserViewer.objects.get(user=user_test)
		self.assertEqual(True , cli_user.has_perm('viewer.permission_viewer'))

	def test_permission_after_disallow(self):
		user_test = User.objects.get(username='5501012634444')
		cli_user = UserViewer.objects.get(user=user_test)
		self.client.post('en/viewer/'+ str(cli_user.id) +'/disallow_user/', {'date_limit_field', '12-10-2016'})
		cli_user = self.viewer.user
		self.assertEqual(False , cli_user.has_perm('viewer.permission_viewer'))