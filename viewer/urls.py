from django.conf.urls import url 

from . import views
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^new/$', views.new, name='new'),

	url(r'^main_page/$', views.main_page, name="main_page"),
	url(r'^to_create_viewer/$', views.to_create_viewer, name="to_create_viewer"),
	url(r'^create_viewer/$', views.create_viewer, name="create_viewer"),
	url(r'^(?P<user_id>\d+)/to_edit_viewer/$', views.to_edit_viewer, name="to_edit_viewer"),
	url(r'^(?P<user_id>\d+)/edit_viewer/$', views.edit_viewer, name="edit_viewer"),
	url(r'^(?P<user_id>\d+)/edit_picture/$', views.edit_picture, name="edit_picture"),
	url(r'^list_viewer/$', views.list_viewer, name="list_viewer"),
	url(r'^(?P<user_id>\d+)/delete_viewer/$', views.delete_viewer, name="delete_viewer"),
	url(r'^search_viewer/$', views.search_viewer, name="search_viewer"),
	url(r'^get_news_by_page/$', views.get_news_by_page, name="get_news_by_page"),
	#url(r'^(?P<news_id>\w+)/(?P<page_c>\w+)/(?P<page_l>\d+)/get_news_by_page/$', views.get_news_by_page, name="get_news_by_page"),
	url(r'^(?P<tag>\w+)/click_tag_news/$', views.click_tag_news, name="click_tag_news"),
	url(r'^add_user_csv/$', views.add_user_csv, name='add_user_csv'),
	url(r'^(?P<user_id>\d+)/disallow_user/$', views.disallow_user, name='disallow_user'),
	url(r'^(?P<user_id>\d+)/allow_user/$', views.allow_user, name='allow_user'),
]