from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
# Create your models here.
class UserViewer(models.Model):  
    user = models.OneToOneField(User)  
    picture = models.ImageField(null=True, blank=True, upload_to= 'profile', default='profile/circleprofile.png')
    limit_time = models.DateField(null=True, blank=True, default=None)
    class Meta:
        permissions = (
            ('permission_viewer', 'Search news read news and post message'),
            ('permission_admin', 'manage user and news program'),
        )

    def __str__(self):  
          return "%s's profile" % self.user  

def create_user_profile(sender, instance, created, **kwargs):  
    if created:  
       profile, created = UserViewer.objects.get_or_create(user=instance)  

post_save.connect(create_user_profile, sender=User) 