#-*- coding: utf-8 -*-
from django import forms

class ViewerForm(forms.Form):
	username = forms.CharField(max_length=30)
	first_name = forms.CharField()
	last_name = forms.CharField()
	password = forms.CharField(max_length=30,widget=forms.PasswordInput()) #render_value=False
	password_confirm = forms.CharField(max_length=30,widget=forms.PasswordInput())

class ViewerEditForm(forms.Form):
	password_old = forms.CharField(max_length=30,widget=forms.PasswordInput()) #render_value=False
	password = forms.CharField(required=False, max_length=30,widget=forms.PasswordInput()) #render_value=False
	password_confirm = forms.CharField(required=False, max_length=30,widget=forms.PasswordInput())

class ViewerPicture(forms.Form):
	ViewerPictureUploader = forms.ImageField()

class UserCSV(forms.Form):
	userUploader = forms.FileField()