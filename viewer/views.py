#-*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views import generic
from datetime import datetime
from django.contrib.auth.models import Permission, User

from datetime import datetime, date, time, timedelta
import datetime, pytz
from .models import UserViewer
from django.contrib.auth.decorators import permission_required, login_required, user_passes_test
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import ViewerForm, ViewerEditForm, ViewerPicture, UserCSV
from newsapp.models import PageList

@login_required(login_url="login/")
def home(request):
	template = 'index2.html'
	context = main_news()
	context['tag'] = 'all'
	return render(request, template, context)

@login_required(login_url="../login/")
def main_page(request):
	context  = {}
	context['tag'] = 'all'
	if request.user.is_authenticated():
		user = request.user
		viewer = UserViewer.objects.get(user= user) # get UserViewer object
		if viewer.limit_time: 
			if viewer.limit_time <= datetime.datetime.now().date(): # if time up to disallow.
				permission = Permission.objects.get(codename='permission_viewer') # get permission
				user.user_permissions.add(permission) # add permission
				user.save()
		if user.is_active: # login success
			if user.has_perm('viewer.permission_admin'):
				context['is_admin'] = True
			elif user.has_perm('viewer.permission_viewer'): # viewer
				context['is_viewer'] = True
				context['allow_post'] = True
			else:
				context['is_viewer'] = True

		context['viewer'] = viewer
		context.update(main_news())
		return render(request, 'index2.html', context)
	else:
		context = main_news()
		return render(request, 'index2.html', context)

def login(request):
	template = 'index2.html'
	context = main_news()
	context['tag'] = 'all'
	return render(request, template, context)	

def logout(request):
	from django.contrib.auth import logout
	logout(request)
	context = main_news()
	context['tag'] = 'all'
	return render(request, 'index2.html', context)

def new(request): # login username and password
	from django.contrib.auth import authenticate, login
	if request.method == 'POST':
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(username=username, password=password)
	context = {}
	if user is not None:
		if user.is_active: # login success
			login(request, user)
			if user.has_perm('viewer.permission_admin'):
				context['is_admin'] = True
			elif user.has_perm('viewer.permission_viewer'): # viewer
				context['is_viewer'] = True
				context['allow_post'] = True
			else:
				context['is_viewer'] = True
			return HttpResponseRedirect(reverse('viewer:main_page')) # go to main page by change url on browser.
		else: # login failure 
			context['authen'] = 'try again.'
	else : # no have this user.
		context['authen'] = 'username or password is incorrect.'
	return render(request, 'index2.html', context)

@permission_required('viewer.permission_admin')
def to_create_viewer(request): # go to create viewer page.
	return render(request, 'create_viewer.html',{})

@permission_required('viewer.permission_admin')
def create_viewer(request): # create new viewer.
	from django.core.files import File
	context = {}
	template_name = "create_viewer.html"
	if request.method == "POST":
		MyViewerForm = ViewerForm(request.POST, request.FILES) # get form
		if MyViewerForm.is_valid():
			if User.objects.filter(username = MyViewerForm.cleaned_data["username"]): # if this username is used already.
				context["error_message"] = "This username is used already."
				return render(request, template_name, context) # back to create user page.
			password = MyViewerForm.cleaned_data["password"]
			password_confirm = MyViewerForm.cleaned_data["password_confirm"]
			if password == password_confirm and not User.objects.filter(username=MyViewerForm.cleaned_data["username"]): # if username is unique and password and password_confirm are the same. 
				new_user = User.objects.create_user(username=MyViewerForm.cleaned_data["username"],
													first_name=MyViewerForm.cleaned_data["first_name"],
													last_name=MyViewerForm.cleaned_data["last_name"],
													password=password)	
				new_user.save()
				permission = Permission.objects.get(codename='permission_viewer') # get permission
				new_user.user_permissions.add(permission) # add permission
				new_user.save()
				new_viewer = UserViewer(user = new_user)
				
				return HttpResponseRedirect(reverse('viewer:list_viewer')) # return to list viewer.
			else: # return error password wrong with password_confirm
				context["error_message"] = "invalid comfirm password"
		else: # form is invalid
			context["error_message"] = "Please insert valid data."
	return render(request, template_name, context)

@login_required
def to_edit_viewer(request, user_id):  # go to edit viewer page and send old date viewer to fill in form.
	viewer = get_object_or_404(UserViewer, pk=user_id) # get UserViewer object
	context = {}
	user = viewer.user
	if user.is_active: # login success
		if user.has_perm('viewer.permission_admin'):
			context['is_admin'] = True
		elif user.has_perm('viewer.permission_viewer'): # viewer
			context['is_viewer'] = True
			context['allow_post'] = True
		else:
			context['is_viewer'] = True

	context['viewer'] = viewer
	return render(request, "edit_viewer.html", context)

@login_required
def to_edit_user(request, user_id): # not use
	viewer = get_object_or_404(UserViewer, pk=user_id)
	return render(request, "edit_usr.html", {'viewer':viewer})

@login_required
def edit_picture(request, user_id): # change only picture viewer.
	viewer = get_object_or_404(UserViewer, pk=user_id) # get UserViewer object	
	if request.method == "POST":
		MyPictureForm = ViewerPicture(request.POST, request.FILES)
		print("POST picture: "+str(MyPictureForm.is_valid()) )
		if MyPictureForm.is_valid():
			print("hello picture")
			if viewer.picture.name != 'profile/circleprofile.png':
				viewer.picture.delete()
			viewer.picture = MyPictureForm.cleaned_data["ViewerPictureUploader"] #upload new picture
			viewer.save()
	return HttpResponseRedirect(reverse('viewer:to_edit_viewer', kwargs={'user_id':viewer.id})) # back to the edit_viewer page.

@login_required
def edit_viewer(request, user_id): # save new data viewer. viewer can change only password and profile picture.
	from django.contrib.auth.models import User
	context = {}
	template_name = "edit_viewer.html"
	viewer = get_object_or_404(UserViewer, pk=user_id) # get UserViewer object
	user = viewer.user # User object from UserViewer
	if request.method == "POST":
		MyEditForm = ViewerEditForm(request.POST, request.FILES)
		if MyEditForm.is_valid():
			password = MyEditForm.cleaned_data["password"]
			password_confirm = MyEditForm.cleaned_data["password_confirm"]
			if user.check_password(MyEditForm.cleaned_data["password_old"]) and (password == password_confirm): # if insert valid password and insert valid new password.
				viewer.user.set_password(password) # save new password
				user.save()
				viewer.save()
				context['viewer'] = viewer
				if user.is_active: # login success
					if user.has_perm('viewer.permission_admin'):
						context['is_admin'] = True
						return HttpResponseRedirect(reverse('viewer:list_viewer')) # return to list viewer.
					elif user.has_perm('viewer.permission_viewer'): # viewer
						context['is_viewer'] = True
						context['allow_post'] = True
					else:
						context['is_viewer'] = True
				return HttpResponseRedirect(reverse('viewer:main_page'))
			else:
				context["error_message"] = "invalid comfirm password"
		else:
			context["error_message"] = "invalid insert data"
	if user.is_active: # login success
		if user.has_perm('viewer.permission_admin'):
			context['is_admin'] = True
		elif user.has_perm('viewer.permission_viewer'): # viewer
			context['is_viewer'] = True
			context['allow_post'] = True
		else:
			context['is_viewer'] = True
	context['viewer'] = viewer
	return 	render(request, template_name, context)

@permission_required('viewer.permission_admin')
def list_viewer(request): # show list viewer user.
	context = {}
	users = []
	for each_user in UserViewer.objects.all():
		if each_user.user.has_perm('viewer.permission_viewer'):
			users.append({'each_user': each_user,"perm": "no_has_perm"})
		else:
			users.append({'each_user': each_user,"perm": "no_has_perm"})
	return render(request, "list_viewer.html", {"users":UserViewer.objects.all()})

@permission_required('viewer.permission_admin')
def delete_viewer(request, user_id): # delete viewer by id
	from django.contrib.auth.models import User
	viewer = get_object_or_404(UserViewer, pk=user_id)
	if viewer.picture:
		if viewer.picture.name != 'profile/circleprofile.png':
			viewer.picture.delete()
	viewer.user.delete()
	viewer.delete()
	return HttpResponseRedirect(reverse('viewer:list_viewer')) # return to list viewer.

@permission_required('viewer.permission_admin')
def search_viewer(request): # search viewer by username, firstname and lastname
	from django.contrib.auth.models import User
	template_name = "list_viewer.html"
	context = {}
	if request.method == "GET" and request.GET["search_viewer"]:
		search_data = request.GET["search_viewer"] # data search
		if User.objects.filter(username = search_data): # if found by username
			user = User.objects.filter(username = search_data)
			viewer = UserViewer.objects.filter(user= user)
			context['users'] = viewer
		elif User.objects.filter(first_name = search_data):# if found by first_name
			user = User.objects.filter(first_name = search_data)
			viewer = UserViewer.objects.filter(user= user)
			context['users'] = viewer
		elif User.objects.filter(last_name = search_data):# if found by last_name
			user = User.objects.filter(last_name = search_data)
			viewer = UserViewer.objects.filter(user= user)
			context['users'] = viewer
		else:  # if not found
			context["error_message"] = "Viewer not found"
	return render(request, template_name, context)

def main_news(tag_name = None, current=1): # use for show news in index2.html only first 6 news
	if tag_name == None: # get all news
		all_news = PageList.objects.all()
	else: # get news filter by tag
		if tag_name == 'tag1':
			tag_name = u'ภาควิชา'
		elif tag_name == 'tag2':
			tag_name = u'กิจกรรม'
		elif tag_name == 'tag3':
			tag_name = u'ทั่วไป'
		elif tag_name == 'tag4':
			tag_name = u'รับสมัครงาน/ฝึกงาน'
		all_news = PageList.objects.filter(tag=tag_name)

	new_news_geten = []
	for each_news in all_news: # loop check no plugin page
		placeholders = list(each_news.page.get_placeholders())
		if placeholders[0].get_plugins_list() and (each_news.prototype == None): # if have plugin
			new_news_geten.append(each_news) # add to new list
	all_news = new_news_geten 

	context = {}
	paginator = Paginator(all_news, 6)
	page = paginator.page(current) # get first 6 news

	context['page_num'] = paginator.page_range
	context['show_news'] = page.object_list
	context['page_now'] = current
	context['num_page_news'] = page
	return context

def get_news_by_page(request):  #, news_id, page_c, page_l):
	template = 'index2.html'
	context = {}
	page = request.GET.get('page')
	tag = request.GET.get('tag')
	context['tag'] = tag
	if tag == 'all':
		context.update(main_news(current=int(page)))
	else:
		context.update(main_news(tag, int(page)))

	if request.user.is_authenticated():
		user = request.user
		if user.is_active: # login success
			if user.has_perm('viewer.permission_admin'):
				context['is_admin'] = True
			elif user.has_perm('viewer.permission_viewer'): # viewer
				context['is_viewer'] = True
				context['allow_post'] = True
			else:
				context['is_viewer'] = True

		viewer = UserViewer.objects.get(user= user) # get UserViewer object
		context['viewer'] = viewer
		return render(request, template, context)
	else:
		return render(request, template, context)

def click_tag_news(request, tag): # get news by tag
	context  = {}
	context['tag'] = tag
	if tag == 'tag1':
		tag_name = u'ภาควิชา'
	elif tag == 'tag2':
		tag_name = u'กิจกรรม'
	elif tag == 'tag3':
		tag_name = u'ทั่วไป'
	elif tag == 'tag4':
		tag_name = u'รับสมัครงาน/ฝึกงาน'
	if request.user.is_authenticated():
		user = request.user
		if user.is_active: # login success
			if user.has_perm('viewer.permission_admin'):
				context['is_admin'] = True
			elif user.has_perm('viewer.permission_viewer'): # viewer
				context['is_viewer'] = True
				context['allow_post'] = True
			else:
				context['is_viewer'] = True

		viewer = UserViewer.objects.get(user= user) # get UserViewer object
		context['viewer'] = viewer
		context.update(main_news(tag_name))
		return render(request, 'index2.html', context)
	else:
		context = main_news(tag_name)
		context.update({'tag':tag})
		return render(request, 'index2.html', context)

@permission_required('viewer.permission_admin')
@login_required
def add_user_csv(request):
	import csv
	from filer.models import File
	if request.method == "POST":
		csvForm = UserCSV(request.POST, request.FILES)
		if csvForm.is_valid():
			csv_file = File.objects.create(owner=request.user,
											original_filename='csv_user',
											file= csvForm.cleaned_data["userUploader"])
			with open(csv_file.path, 'rt') as f:
				reader = csv.DictReader(f)
				for row in reader:
					password = str(row['username'])[1:]
					if not(User.objects.filter(username=row['username']) or User.objects.filter(first_name=row['firstname']) ):
						new_user = User.objects.create_user(username=row['username'],
															first_name=row['firstname'],
															last_name=row['lastname'],
															password=password)	
						new_user.save()
						permission = Permission.objects.get(codename='permission_viewer') # get permission
						new_user.user_permissions.add(permission) # add permission
						new_user.save()
						new_viewer = UserViewer(user = new_user)

			csv_file.delete()

	return HttpResponseRedirect(reverse('viewer:list_viewer'))

@permission_required('viewer.permission_admin')
@login_required
def disallow_user(request, user_id): # limit permission uesr viewer
	if request.method == "POST":
		limit_data = request.POST["date_limit_field"].split("-") # get limit date
		limit_data = [ int(i) for i in limit_data  ]
		limit_date = datetime.datetime(limit_data[2], limit_data[1], limit_data[0]) # create limit datetime.

		viewer = get_object_or_404(UserViewer, pk=int(user_id)) # get viewer instance
		viewer.limit_time = limit_date # set limit date
		viewer.save()
		permission = Permission.objects.get(codename='permission_viewer') # get permission viewer
		viewer.user.user_permissions.remove(permission) # delete permission 
		viewer.save()

	return HttpResponseRedirect(reverse('viewer:list_viewer'))

@permission_required('viewer.permission_admin')
@login_required
def allow_user(request, user_id): # return permission to user viewer
	viewer = get_object_or_404(UserViewer, pk=int(user_id)) # get viewer instance
	permission = Permission.objects.get(codename='permission_viewer') # get permission viewer
	if not viewer.user.has_perm('viewer.permission_viewer'):
		viewer.user.user_permissions.add(permission) # add permission 
		viewer.limit_time = None
		viewer.save()

	return HttpResponseRedirect(reverse('viewer:list_viewer'))