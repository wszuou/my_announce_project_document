from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from polls_plugin.models import PollPlugin 
from django.utils.translation import ugettext as _ 

class CMSPollPlugin(CMSPluginBase):
	model = PollPlugin
	module = _("Polls")
	name = ("Poll Plugin")
	render_template = "djangocms_polls/poll_plugin.html"

	def render(self, context, instance, placeholder):
		context.update({'instance': instance})
		return context

plugin_pool.register_plugin(CMSPollPlugin)